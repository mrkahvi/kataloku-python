from flask import request, json
from slugify import slugify
from kataloku_api.utils.exceptions import InvalidUsage
from kataloku_api import app
from kataloku_api.utils.rest_handler import request_failed
from kataloku_api.models import db, Category, User, BusinessTag, Business, BusinessCategory, Picture, Item, BusinessViewLog, NearbyLog, NavigationLog, Review
from kataloku_api.configs.rest_config import BASE_URL
from kataloku_api.apicalls.review import get_business_review, check_user_has_reviewed, get_detail_review
from werkzeug.utils import secure_filename
from datetime import  datetime
import os
from kataloku_api.utils.utilities import verify_post_params, echo_response, enum
from kataloku_api.configs.global_const import DEFAULT_QUERY_OFFSET, DEFAULT_RETURN_COUNT
from config import SERVER_URL, AVATAR_UPLOAD_FOLDER, AVATAR_BASE_DIR, SHOWCASE_UPLOAD_FOLDER, SHOWCASE_BASE_DIR
from config import dbCon
import MySQLdb
import pprint
from sqlalchemy.sql import func

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

app.config['UPLOAD_FOLDER'] = AVATAR_UPLOAD_FOLDER
app.config['SHOWCASE_UPLOAD_FOLDER'] = SHOWCASE_UPLOAD_FOLDER

__RESOURCE = 'business'


# GET BUSINESS CATEGORIES
def get_categories():
    if request.method == 'GET':
        lim = request.args.get('limit')
        off = request.args.get('offset')

        results = []
        if lim and off:
            results = BusinessCategory.query.limit(lim).offset(off).all()
        else:
            results = BusinessCategory.query.all()

        json_results = []
        for result in results:
            d = {'id': result.id,
                 'name': result.name,
                 'slug': result.slug,
                 'type': result.type_id
                 }
            json_results.append(d)
        obj = {'status': 200, 'message': 'success', 'categories': json_results}

        return obj


def get_business(userkey, business_id):
    # check userkey
    user = is_userkey_valid(userkey)
    if not user or userkey is None:
        return echo_response(401, 'missing user key')

    result = Business.query.filter_by(id=business_id).first()
    if result:
        showcases = Picture.query.filter_by(business_id=business_id).order_by(Picture.id.desc()).limit(3)

        business_showcases = []
        i = 0
        for showcase in showcases:
            if i < 3:
                if showcase.picture is not None:
                    showcase_url = SERVER_URL + showcase.picture
                else:
                    showcase_url = ''
                obj_showcase = {
                    'id': showcase.id,
                    'picture': showcase_url
                }
                business_showcases.append(obj_showcase)
                i += 1
            else:
                break

        if result.avatar is not None:
            avatar_url = SERVER_URL + result.avatar
        else:
            avatar_url = ''

        business = business_to_json(user.id, result)

        obj = {'status': 200, 'message': 'success', 'business': business}
        return obj
    else:
        return echo_response(204, "business not found")


def get_business_json_by_owner(user_id):
    # user_id = request.form.get('user_id', '')
    json_result = {}

    result = Business.query.filter_by(user_id=user_id).first()
    if result:
        category = BusinessCategory.query.filter_by(id=result.category_id).first()
        obj_cat = {'id': category.id,
                   'name': category.name,
                   'slug': category.slug,
                   'type_id': category.type_id
                   }

        json_result = {
            'id': result.id,
            'name': result.business_name,
            'description': result.description,
            'category': obj_cat,
            'address': result.address,
            'city': result.city,
            'postal_code': result.postal_code,
            'phone': result.phone,
            'avatar': SERVER_URL + (result.avatar or ''),
            'website': result.website,
            'province': result.province,
            'tag': result.tags,
            'first_use': result.first_use,
            'operation_days': result.operation_days,
            'lat': result.latitude or 0.0,
            'lng': result.longitude or 0.0,
            'created_at': result.create_date}

    return json_result


def nearby_business_to_json(user_id, business):
    # category
    category = BusinessCategory.query.filter_by(id=business.category_id).first()
    if category is not None:
        obj_cat = {
            'id': category.id,
            'name': category.name,
            'slug': category.slug,
            'type_id': category.type_id
        }
    else:
        obj_cat = {
            'name': 'aaa'
        }


    # showcase
    showcases = Picture.query.filter_by(business_id=business.id).order_by(Picture.id.desc()).limit(3)
    business_showcases = []
    i = 0
    for showcase in showcases:
        if i < 3:
            if showcase.picture is not None:
                showcase_url = SERVER_URL + showcase.picture
            else:
                showcase_url = ''
            obj_showcase = {
                'id': showcase.id,
                'picture': showcase_url
            }
            business_showcases.append(obj_showcase)
            i += 1
        else:
            break


    # items
    items = Item.query.filter_by(business_id=business.id).order_by(Item.id.desc()).limit(3)
    business_items = []
    i = 0
    for item in items:
        if i < 3:
            if item.photo is not None:
                item_photo = SERVER_URL + item.photo
            else:
                item_photo = ''

            tags = []
            entity_tags = item.tags
            for entity_tag in entity_tags:
                tags.append(entity_tag.name)

            obj_item = {
                'id': item.id,
                'tags': tags,
                'description': item.description,
                'category_id': item.category_id,
                'business_id': item.business_id,
                'picture': item_photo,
                'type': item.type
            }
            business_items.append(obj_item)
            i += 1
        else:
            break

    tag_business = []
    # for tag in business.tags:
    #     obj_item = {
    #         'id': tag.id,
    #         'name': tag.name
    #     }
    #     tag_business.append(obj_item)
    #     i += 1


    # tags = BusinessTag.query.filter_by(business_id=business.id).all()
    #
    # tag_business = []
    # for tag in tags:
    #     obj_item = {
    #         'id': tag.id,
    #         'name': tag.name
    #     }
    #     tag_business.append(obj_item)
    #     i += 1

    json_result = {'id': business.id,
                   'name': business.business_name,
                   'description': business.description,
                   'category': obj_cat,
                   'address': business.address,
                   'city': business.city,
                   'postal_code': business.postal_code,
                   'phone': business.phone,
                   'avatar': SERVER_URL + (business.avatar or ''),
                   'website': business.website,
                   'province': business.province,
                   'tag': tag_business,
                   'operation_days': business.operation_days,
                   'lat': business.latitude or 0.0,
                   'lng': business.longitude or 0.0,
                   'distance': business.distance,
                   'created_at': business.create_date,
                   'showcases': business_showcases,
                   'items': business_items
                   }

    # if owner, show private informations
    if user_id == business.user_id:
        json_result['first_use'] = business.first_use

    return json_result


def business_to_json(user_id, business):
    avg = db.session.query(func.avg(Review.rating).label('average')).filter(Review.business_id==business.id).scalar()

    query = "SELECT avg(Review.rating) AS average FROM Review WHERE Review.business_id = " + str(business.id)
    cur = dbCon.cursor(MySQLdb.cursors.DictCursor)
    cur.execute(query)
    result = cur.fetchone()

    # category
    category = BusinessCategory.query.filter_by(id=business.category_id).first()
    obj_cat = {'id': category.id,
               'name': category.name,
               'slug': category.slug,
               'type_id': category.type_id
               }

    # showcase
    showcases = Picture.query.filter_by(business_id=business.id).order_by(Picture.id.desc()).limit(3)
    business_showcases = []
    i = 0
    for showcase in showcases:
        if i < 3:
            if showcase.picture is not None:
                showcase_url = SERVER_URL + showcase.picture
            else:
                showcase_url = ''
            obj_showcase = {
                'id': showcase.id,
                'picture': showcase_url
            }
            business_showcases.append(obj_showcase)
            i += 1
        else:
            break

    # items
    items = Item.query.filter_by(business_id=business.id).order_by(Item.id.desc()).limit(3)
    business_items = []

    i = 0
    for item in items:
        if i < 3:
            if item.photo is not None:
                item_photo = SERVER_URL + item.photo
            else:
                item_photo = ''


            tags = []
            entity_tags = item.tags
            for entity_tag in entity_tags:
                tags.append(entity_tag.name)

            obj_item = {
                'id': item.id,
                'tags': tags,
                'description': item.description,
                'category_id': item.category_id,
                'business_id': item.business_id,
                'picture': item_photo,
                'type': item.type
            }
            business_items.append(obj_item)
            i += 1
        else:
            break

    is_bookmarked = False

    bookmark_user = User.query.filter(User.bookmarked_business.any(id=business.id)).all()

    if len(bookmark_user) > 0:
        is_bookmarked = True

    json_result = {
        'id': business.id,
        'name': business.business_name,
        'description': business.description,
        'category': obj_cat,
        'address': business.address,
        'city': business.city,
        'postal_code': business.postal_code,
        'phone': business.phone or '',
        'avatar': SERVER_URL + (business.avatar or ''),
        'website': business.website or '',
        'province': business.province or '',
        'tag': business.tags,
        'email': business.email,
        'operation_days': business.operation_days,
        'lat': business.latitude or 0.0,
        'lng': business.longitude or 0.0,
        'created_at': business.create_date,
        'showcases': business_showcases,
        'items': business_items,
        'is_bookmarked' : is_bookmarked,
        'rating' : float(result["average"] or 0)
    }

    # if owner, show private informations
    if user_id == business.user_id:
        json_result['first_use'] = business.first_use

    return json_result


def register_business(userkey):
    # check userkey
    user = is_userkey_valid(userkey)
    if not user or userkey is None:
        return echo_response(401, 'missing user key')


    # check required params
    any_missing = verify_post_params(['user_id', 'business_name',
                                      'business_category', 'address', 'city'])

    if any_missing is not None:
        return any_missing

    user_id = request.form.get('user_id', '')
    business_name = request.form.get('business_name', '')
    business_category = request.form.get('business_category', '')
    business_type = request.form.get('business_type', '')
    description = request.form.get('description', '')
    address = request.form.get('address', '')
    city = request.form.get('city', '')
    postal_code = request.form.get('postal_code', '')
    lat = request.form.get('lat', '')
    lng = request.form.get('lng', '')
    phone = request.form.get('phone', '')
    email = request.form.get('email', '')
    json_tag = request.form.get('tag', '')
    website = request.form.get('website', '')

    # business profile pic
    file_pic = None
    if 'picture' in request.files:
        file_pic = request.files['picture']


    entity = Business.query.filter_by(user_id=user_id).first()

    if not entity:
        try:
            entity = Business()
            entity.user_id = user_id
            entity.business_name = business_name
            slug = slugify(business_name)
            others = Business.query.filter_by(slug=slug)
            count = others.count()
            if count > 1:
                entity.slug = slug + '-' + str(count)
            else:
                entity.slug = slug

            entity.type_id = business_type
            entity.description = description
            entity.category_id = business_category
            entity.address = address
            entity.city = city
            entity.latitude = lat
            entity.longitude = lng
            entity.postal_code = postal_code
            entity.phone = phone
            entity.first_use = 1
            entity.email = email
            entity.website = website

            # TODO: set tags
            tags = json.loads(json_tag)
            for tag in tags:
                new_tag = BusinessTag()
                new_tag.name = tag
                db.session.add(new_tag)

            # entity.tags = tags_string


            if file_pic is not None:
                filename = secure_filename(file_pic.filename)
                file_pic.save(os.path.join(app.config['AVATAR_UPLOAD_FOLDER'], filename))
                entity.avatar = os.path.join(AVATAR_BASE_DIR, filename)

            # add business
            db.session.add(entity)
            db.session.commit()


            # add showcase

            files = request.files
            files_array = []
            for x in range(0, len(files)):
                if 'showcase_pic' + str(x) in request.files:
                    file_item = files['showcase_pic' + str(x)]
                    if file_item is not None:
                        files_array.append(file_item)

            for file_item in files_array:
                try:
                    if file_item is not None:
                        file = file_item
                        # if file and allowed_file(file.filename):
                        filename = secure_filename(file.filename)
                        file.save(os.path.join(app.config['SHOWCASE_UPLOAD_FOLDER'], filename))
                        showcase = Picture()
                        showcase.business_id = entity.id
                        showcase.picture = os.path.join(SHOWCASE_BASE_DIR, filename)
                        db.session.add(showcase)
                        db.session.commit()
                except InvalidUsage:
                    return echo_response(400, 'failed')

            business = business_to_json(user.id, entity)
            obj = {'status': 200, 'message': 'success', 'business': business}

            return obj
        except InvalidUsage:
            obj = {'status': 400, 'message': 'failed'}
            return obj
            #return echo_response(400, 'failed')
            # except:
            #     return request_failed()
    else:
        obj = {'status': 204, 'message': 'failed. This user already has a business '}
        return obj
        #return echo_response(204, 'failed. This user already has a business ')


def update_business(userkey, business_id):
    if not business_id:
        return echo_response(401, 'business id invalid')

        # check userkey
    user = is_userkey_valid(userkey)
    if not user or userkey is None:
        return echo_response(401, 'missing user key')

    business = Business.query.filter_by(id=business_id).first()
    if business is None:
        return echo_response(204, 'business not found')

    business_name = request.form.get('business_name')
    business_category = request.form.get('business_category')
    business_type = request.form.get('business_type')
    description = request.form.get('description')
    address = request.form.get('address')
    city = request.form.get('city')
    province = request.form.get('province')
    postal_code = request.form.get('postal_code')
    lat = request.form.get('lat')
    lng = request.form.get('lng')
    phone = request.form.get('phone')
    email = request.form.get('email')
    json_tag = request.form.get('tag')
    website = request.form.get('website')
    operation_days = request.form.get('operation_days')
    first_use = request.form.get('first_use')


    # business profile pic
    file_pic = None
    if 'picture' in request.files:
        file_pic = request.files['picture']

    try:
        entity = business
        if not (business_name is None):
            entity.name = business_name
            slug = slugify(business_name)
            others = Business.query.filter_by(slug=slug)
            if others:
                entity.slug = slug + '-' + str(others.count())
            else:
                entity.slug = slug

        if not (business_type is None): entity.category.type_id = business_type
        if not (description is None): entity.description = description
        if not (business_category is None): entity.category_id = business_category
        if not (address is None): entity.address = address
        if not (city is None): entity.city = city
        if not (province is None): entity.province = province
        if not (lat is None): entity.latitude = lat
        if not (lng is None): entity.longitude = lng
        if not (postal_code is None): entity.postal_code = postal_code
        if not (phone is None): entity.phone = phone
        if not (first_use is None): entity.first_use = 1
        if not (email is None): entity.email = email
        if not (website is None): entity.website = website
        if not (operation_days is None): entity.operation_days = operation_days

        if file_pic is not None:
            filename = secure_filename(file_pic.filename)
            file_pic.save(os.path.join(app.config['AVATAR_UPLOAD_FOLDER'], filename))
            entity.picture = os.path.join(AVATAR_BASE_DIR, filename)


        # TODO: set tags
        if not (json_tag is None):
            tags = json.loads(json_tag)
            for tag in tags:
                new_tag = BusinessTag()
                new_tag.name = tag
                db.session.add(new_tag)


        # add business
        db.session.add(entity)
        db.session.commit()

        business = business_to_json(user.id, entity)
        obj = {'status': 200, 'message': 'success', 'business': business}

        return obj

    except InvalidUsage:
        return echo_response(400, 'failed')
        # except:
        #     return request_failed()


def show_nearby(userkey, distance, lat, lng, page):
    if not lat:
        return echo_response(401, 'missing latitude')
    if not lng:
        return echo_response(401, 'missing longitude')

    # check userkey
    user = is_userkey_valid(userkey)
    if (not user or userkey is None) and userkey != '000000000':
        return echo_response(401, 'missing user key')

    lim = request.args.get('limit')
    off = request.args.get('offset')
    cat = request.args.get('cat');

    # TODO: query it with nearby algoritm
    end = 20 * (int(page)+1)
    start = int(end) - 20

   # query = "SELECT *, ( 6371 * acos( cos( radians(" + lat + " ) ) * cos( radians(Business.latitude) ) * cos( radians( Business.longitude ) - radians(" + lng + ") ) + sin( radians(" + lat + " ) ) * sin(radians(Business.latitude)) ) ) AS distance  From Business HAVING distance <= " + distance + " OR distance = 0 ORDER BY distance ASC LIMIT " + str(start) + " , " + str(end)
    if cat:
        query = "SELECT *, ( 6371 * acos( cos( radians(" + lat + " ) ) * cos( radians(Business.latitude) ) * cos( radians( Business.longitude ) - radians(" + lng + ") ) + sin( radians(" + lat + " ) ) * sin(radians(Business.latitude)) ) ) AS distance  From Business WHERE Business.category_id=" + cat + " HAVING distance <= " + distance + " OR distance = 0 ORDER BY distance ASC LIMIT " + str(start) + " , " + str(end)
        #query = "SELECT *, ( 6371 * acos( cos( radians(" + lat + " ) ) * cos( radians(Business.latitude) ) * cos( radians( Business.longitude ) - radians(" + lng + ") ) + sin( radians(" + lat + " ) ) * sin(radians(Business.latitude)) ) ) AS distance From Business WHERE Business.category_id=" + cat + " HAVING distance <= " + distance + " OR distance = 0 ORDER BY distance ASC"
    else:
        query = "SELECT *, ( 6371 * acos( cos( radians(" + lat + " ) ) * cos( radians(Business.latitude) ) * cos( radians( Business.longitude ) - radians(" + lng + ") ) + sin( radians(" + lat + " ) ) * sin(radians(Business.latitude)) ) ) AS distance  From Business HAVING distance <= " + distance + " OR distance = 0 ORDER BY distance ASC LIMIT " + str(start) + " , " + str(end)


    results = db.engine.execute(query)

    json_results = []

    i = 0
    for result in results:
        user = User()
        d = nearby_business_to_json(user.id, result)
        json_results.append(d)
        if result.user_id != user.id:
            view_log = NearbyLog()
            view_log.date = datetime.now()
            view_log.user = user
            view_log.business_id = result.id
            view_log.distance = distance
            view_log.order_id = i + start
            db.session.add(view_log)
            i = i + 1
    db.session.commit()

    if len(json_results) > 0:
        obj = {'status': 200, 'message': 'success', 'business': json_results}
    else:
        obj = {'status': 204, 'message': 'no business near here'}
    return obj


def detail_business(userkey, business_id, ref):
    # check userkey
    user = is_userkey_valid(userkey)
    if (not user or userkey is None) or userkey == '000000000':
        return echo_response(401, 'missing user key')

    business = Business.query.filter_by(id=business_id).first()

    if business is None:
        return echo_response(204, 'business not found')
    else:
        data = business_to_json(user.id, business)

        has_reviewed = check_user_has_reviewed(user.id, business_id)
        my_review = get_detail_review(user.id, business_id)

        review = get_business_review(business_id, 3)

        if user.id != business.user_id:
            view_log = BusinessViewLog()
            view_log.date = datetime.now()
            view_log.user = user
            view_log.business = business
            view_log.ref = ref
            db.session.add(view_log)
        db.session.commit()

        data['reviews'] = review
        data['has_reviewed'] = has_reviewed
        data['my_review'] = my_review
        obj = {'status': 200, 'message': 'success', 'business': data}
        return obj

def navigation_log(id, business_id, distance):
    try:
      nav_log = NavigationLog()
      nav_log.date = datetime.now()
      nav_log.user_id = id
      nav_log.business_id = business_id
      nav_log.distance = round(float(distance), 2)
      db.session.add(nav_log)
      db.session.commit()

      result={"status" : 200,  "message" : 'success'}

      return result
    except InvalidUsage:
      result={"status" : 400,  "message" : "error"}
      return result

def get_business_json_by_id(business_id):
    json_result = {}

    result = Business.query.filter_by(id=business_id).first()

    is_bookmarked = False

    bookmark_user = User.query.filter(User.bookmarked_business.any(id=result.id)).all()

    if len(bookmark_user) > 0:
        is_bookmarked = True

    if result:
        json_result = {
            'id': result.id,
            'name': result.business_name,
            'description': result.description,
            'address': result.address,
            'city': result.city,
            'zip_code': result.postal_code,
            'phone': result.phone,
            'picture': (result.avatar or ''),
            'website': result.website,
            'brandpage': result.website,
            'email': result.email,
            'tag': result.tags,
            'first_use': result.first_use,
            'lat': result.latitude,
            'lng': result.longitude,
            'created_at': result.create_date,
            'is_bookmarked' : is_bookmarked,
            'bookmark_count' : len(bookmark_user)
        }

    return json_result


def get_showcase(userkey, business_id):
    # check userkey
    user = is_userkey_valid(userkey)
    if not user or userkey is None:
        return echo_response(401, 'missing user key')

    result = Business.query.filter_by(id=business_id).first()
    if result:
        showcases = Picture.query.filter_by(business_id=business_id).order_by(Picture.id.desc()).limit(3)
        if showcases:
            business_showcases = []
            i = 0
            for showcase in showcases:
                if showcase.picture is not None:
                    showcase_url = SERVER_URL + showcase.picture
                else:
                    showcase_url = ''
                obj_showcase = {
                    'id': showcase.id,
                    'picture': showcase_url
                }
                business_showcases.append(obj_showcase)
                i += 1

            obj = {'status': 200, 'message': 'success', 'showcases': business_showcases}
            return obj
        else:
            obj = {'status': 204, 'message':  "there are no showcases"}

    else:
        obj = {'status': 204, 'message':  "business not found"}

    return obj

def upload_showcase(userkey, business_id):
    # check userkey
    if not is_userkey_valid(userkey) or userkey is None:
        return echo_response(401, 'missing user key')

    first_time = request.form.get('first_time')

    if not first_time:
        if not business_id:
            return echo_response(401, 'business id invalid')

        business = Business.query.filter_by(id=business_id).first()
        if business is None:
            return echo_response(204, 'business not found')
    else:
        business_id = 0

    entity = Picture()

    if 'picture' in request.files:
        file_pic = request.files['picture']

        try:
            if file_pic is not None:
                filename = secure_filename(file_pic.filename)
                file_pic.save(os.path.join(app.config['SHOWCASE_UPLOAD_FOLDER'], filename))
                entity.picture = os.path.join(SHOWCASE_BASE_DIR, filename)
                entity.business_id = business_id  # if first time, business_id = 0

            db.session.add(entity)
            db.session.commit()
            obj = {'status': 200, 'message': 'success', 'showcase_id': entity.id,
                   'image_url': SERVER_URL + entity.picture}

            return echo_response(200, 'success')
        except InvalidUsage:
            return echo_response(400, 'failed')

    else:
        return echo_response(204, 'no image uploaded')


def bookmark_business(userkey, business_id):
     #check userkey
    if not is_userkey_valid(userkey) or userkey is None:
        return echo_response(401, 'missing user key')

    try:
        user = User.query.filter_by(api_key=userkey).first()
        business = Business.query.get(int(business_id))
        bookmark_user = User.query.filter(User.bookmarked_business.any(id=business.id)).all()
        bookmarked_business = user.bookmarked_business

        if user in bookmark_user:
            bookmarked_business.remove(business)
        else:
            bookmarked_business.append(business)
        user.bookmarked_business = bookmarked_business
        db.session.commit()

        obj = get_business_json_by_id(business_id)
        result = {"status": 200, 'message': 'success', 'business': obj}
        return result
    except InvalidUsage:
        return echo_response(400, 'failed')

def get_bookmarked_business(userkey):
    businesses = []
    try:
        user = User.query.filter_by(api_key=userkey).first()
        bookmarked_businesses = user.bookmarked_business
        for bookmarked_business in bookmarked_businesses:
            obj = {
                'id': bookmarked_business.id,
                'name': bookmarked_business.business_name,
            }
            businesses.append(obj)

    except InvalidUsage:
        return echo_response(400, 'failed')

    result = {
        "status": 200,
        "business" : businesses
    }

    return result


# (BASE_URL + __RESOURCE + '/delete_null', methods=['GET'])
def delete_null():
    businesses = Business.query.filter_by(business_name=None)
    for business in businesses:
        db.session.delete(business)

    db.session.commit()
    return 'deleted'


def set_showcase_images(business_id, showcase_ids):
    for showcase_id in showcase_ids:
        new_image = Picture.query.filter_by(id=showcase_id).first()
        new_image.business_id = business_id

        db.session.add(new_image)
        db.session.commit()


def is_business_exist(business_id):
    entity = Business.query.filter_by(id=business_id).first()
    if entity is None:
        return False
    else:
        return True


def is_userkey_valid(userkey):
    new_user = User.query.filter_by(api_key=userkey).first()
    return new_user
