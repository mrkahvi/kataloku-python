from kataloku_api.models import Item, Business
from flask import request, json
from datetime import datetime, date, timedelta
from kataloku_api.utils.exceptions import InvalidUsage
from  kataloku_api.dbhelper import offering
from kataloku_api import app
from kataloku_api.models import User, db, Category, SearchLog, Offering, ItemListViewLog, ItemViewLog, ItemTag
from kataloku_api.configs.rest_config import BASE_URL
from werkzeug.utils import secure_filename
from kataloku_api.configs import global_const
import os
from pymysql.err import IntegrityError
from kataloku_api.utils.utilities import verify_post_params, echo_response
from config import SERVER_URL, ITEM_UPLOAD_FOLDER, ITEM_BASE_DIR
from config import dbCon
import MySQLdb

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

app.config['ITEM_UPLOAD_FOLDER'] = ITEM_UPLOAD_FOLDER

__RESOURCE = 'item'


# Required APIs:
# 1. Add Item -ok
# 2. Get Item Categories -ok
# 3. Show Business Item -ok
# 4. Update Item -ok
# 5. Search Nearby
# 6. Show Detail


# (BASE_URL + __RESOURCE + '/add/<userkey>', methods=['POST'])
def add_item(userkey):
    # check userkey
    if not is_userkey_valid(userkey) or userkey is None:
        return echo_response(401, 'missing user key')

    any_missing = verify_post_params(['business_id', 'category_id'])

    # check required params
    if any_missing is not None:
        return any_missing

    business_id = request.form.get("business_id")
    category_id = request.form.get("category_id")
    description = request.form.get("description", '')
    json_tag = request.form.get("tag", '')

    file_pic = None
    if 'photo' in request.files:
        file_pic = request.files['photo']

    entity_tags = []
    if not (json_tag is None):
        tags = json.loads(json_tag)
        for tag in tags:
            exist_tag = ItemTag.query.filter_by(name=tag).first()
            new_tag = ItemTag()
            new_tag.name = tag


            if exist_tag is None:
                db.session.add(new_tag)
                entity_tags.append(new_tag)
            else:
                entity_tags.append(exist_tag)

    # for entity_tag in entity_tags:
    #     print str(entity_tag.id) + ' ' + entity_tag.name

    try:
        entity = Item()
        entity.business_id = business_id
        entity.description = description
        entity.category_id = category_id
        entity.tags = entity_tags

        if file_pic is not None:
            filename = secure_filename(file_pic.filename)
            file_pic.save(os.path.join(app.config['ITEM_UPLOAD_FOLDER'], filename))
            entity.photo = os.path.join(ITEM_BASE_DIR, filename)

        db.session.add(entity)
        db.session.commit()

        obj = get_item_json(entity)
        result = {"status": 200, 'message': 'success', 'item': obj}
        return result
    except InvalidUsage:
        return echo_response(400, 'failed')


# (BASE_URL + __RESOURCE + '/update/<userkey>', methods=['POST'])
def update_item(userkey):
    # check userkey
    if not is_userkey_valid(userkey) or userkey is None:
        return echo_response(401, 'missing user key')

    any_missing = verify_post_params(['item_id'])

    # check required params
    if any_missing is not None:
        return any_missing

        # check existing
    item_id = request.form.get('item_id')
    entity = Item.query.filter_by(id=item_id).first()

    if entity is None:
        return echo_response(204, 'item not found')

    category_id = request.form.get("category_id")
    description = request.form.get("description", '')
    json_tag = request.form.get('tag')

    file_pic = None
    if 'photo' in request.files:
        file_pic = request.files['photo']

    entity_tags = []
    if not (json_tag is None):
        tags = json.loads(json_tag)
        #tags = request.json;

        for tag in tags:
            exist_tag = ItemTag.query.filter_by(name=tag).first()
            new_tag = ItemTag()
            new_tag.name = tag


            if exist_tag is None:
                db.session.add(new_tag)
                entity_tags.append(new_tag)
            else:
                entity_tags.append(exist_tag)

    try:
        if not (category_id is None): entity.category_id = category_id
        entity.tags = entity_tags
        if not (description is None): entity.description = description

        if file_pic is not None:
            filename = secure_filename(file_pic.filename)
            file_pic.save(os.path.join(app.config['ITEM_UPLOAD_FOLDER'], filename))
            entity.photo = os.path.join(ITEM_BASE_DIR, filename)

        db.session.add(entity)
        db.session.commit()

        obj = get_item_json(entity)
        result = {"status": 200, 'message': 'success', 'item': obj}
        return result
    except InvalidUsage:
        return echo_response(400, 'failed')


# GET ITEM CATEGORIES
# (BASE_URL + __RESOURCE + '/categories', methods=['GET'])
def get_item_categories():
    lim = request.args.get('limit')
    off = request.args.get('offset')

    results = []
    if lim and off:
        results = Category.query.limit(lim).offset(off).all()
    else:
        results = Category.query.all()

    json_results = []
    for result in results:
        d = {'id': result.id,
             'name': result.name,
             'slug': result.slug,
             'description': result.description
             }
        json_results.append(d)
    obj = {'status': 200, 'message': 'success', 'categories': json_results}

    return obj


# (BASE_URL + __RESOURCE + '/list/<userkey>/<business_id>', methods=['GET'])
def get_business_items(userkey, business_id, ref):
    user = is_userkey_valid(userkey)
    if not user or userkey is None:
        return echo_response(401, 'missing user key')

    lim = request.args.get('limit')
    off = request.args.get('offset')

    results = []
    if lim and off:
        results = Item.query.filter_by(business_id=business_id).limit(3).offset(off).all()
    else:
        results = Item.query.filter_by(business_id=business_id). \
            order_by(Item.id.desc()).limit(global_const.DEFAULT_RETURN_COUNT).all()

    json_results = []

    if results:
        for result in results:
            if result.photo is not None:
                photo = SERVER_URL + result.photo
            else:
                photo = ''

            business = Business.query.get(result.business_id)
            if business.user_id != user.id:
                itemlist_view_log = ItemListViewLog()
                itemlist_view_log.user_id = user.id
                itemlist_view_log.date = datetime.now()
                itemlist_view_log.ref = ref
                itemlist_view_log.business_id = business.id
                db.session.add(itemlist_view_log)

            tags = []
            entity_tags = result.tags
            for entity_tag in entity_tags:
                tags.append(entity_tag.name)

            d = {'id': result.id,
                 'tag': tags,
                 'description': result.description,
                 'category_id': result.category_id,
                 'business_id': result.business_id,
                 'photo': photo,
                 'type': result.type
                 }
            # d = {'id': result.id,
            #     'name': result.name,
            #     'price': result.price,
            #     'description': result.description,
            #     'category_id': result.category_id,
            #     'business_id': result.business_id,
            #     'unit': result.unit,
            #     'photo': photo,
            #     'type': result.type
            # }

            offer = offering.detail_offering(result.id)

            if offer is None:
                d['has_offering'] = False
            else:
                d['has_offering'] = True
                d['offering'] = offer

            json_results.append(d)
        db.session.commit()
        obj = {'status': 200, 'message': 'success', 'items': json_results}
        return obj
    else:
        return echo_response(204, 'There are no item yet.');


# (BASE_URL + __RESOURCE + '/search/nearby/<userkey>/<keyword>/<distance>/<lat>/<lng>/<page>', methods=['GET'])
def search_nearby(userkey, keyword, distance, lat, lng, page):
    end = 20 * (int(page) + 1)
    start = int(end) - 20

    query = "SELECT Item.id AS item_id, Item.description AS item_description, Business.id AS business_id, Item.photo AS photo, Business.business_name AS business_name, address, longitude, latitude, ( 6371 * acos( cos( radians(" + lat + " ) ) * cos( radians(Business.latitude) ) * cos( radians( Business.longitude ) - radians(" + lng + ") ) + sin( radians(" + lat + " ) ) * sin(radians(Business.latitude)) ) ) AS distance FROM Item LEFT JOIN Business ON Item.business_id = Business.id WHERE Item.description LIKE '%" + keyword + "%' HAVING distance <= " + distance + " OR distance = 0 ORDER BY distance ASC LIMIT " + str(
        start) + " , " + str(end)

    cur = dbCon.cursor(MySQLdb.cursors.DictCursor)
    cur.execute(query)
    results = cur.fetchall()

    items = []

    i = 0
    for row in results:
        current_item = Item.query.filter_by(id=row['item_id']).first()

        tags = []
        entity_tags = current_item.tags
        for entity_tag in entity_tags:
            tags.append(entity_tag.name)
        item = {
            'item_id': row['item_id'] or '',
            'description': row['item_description'] or '',
            'tag':tags,
            'user_id': row['business_id'] or '',
            'business_name': row['business_name'] or '',
            'lat': row['latitude'] or '',
            'lng': row['longitude'] or '',
            'distance': row['distance'] or '',
            'address': row['address'] or '',
            'photo': SERVER_URL + (row['photo'] or ''),
            'distance': distance or ''
        }
        items.append(item)

        item = Item.query.get(row["item_id"])
        user = is_userkey_valid(userkey)
        business = Business.query.get(row["business_id"])

        if business.user_id != user.id:
            view_log = SearchLog()
            view_log.date = datetime.now()
            view_log.user = user
            view_log.business_id = item.business_id
            view_log.keyword = keyword
            view_log.distance = distance
            view_log.order_id = i + start
            db.session.add(view_log)
            i = i + 1

    db.session.commit()

    if len(items) > 0:
        return {'status': 200, 'results': items}
    else:
        return {'status': 204, 'results': []}


def get_item_detail(userkey, item_id, ref):
    user = is_userkey_valid(userkey)
    if not user or userkey is None:
        return echo_response(401, 'missing user key')

    result = Item.query.filter_by(id=item_id).first()

    if result is not None:
        offer = offering.detail_offering(result.id)
        item = get_item_json(result)

        if offer is None:
            item['has_offering'] = False
        else:
            item['has_offering'] = True
            item['offering'] = offer

        business = Business.query.get(result.business_id)

        if business.user_id != user.id:
            item_view_log = ItemViewLog()
            item_view_log.user_id = user.id
            item_view_log.date = datetime.now()
            item_view_log.ref = ref
            item_view_log.item_id = result.id
            db.session.add(item_view_log)

        db.session.commit()

        obj = {
            'status': 200,
            'message': 'success',
            'item': item
        }
        return obj
    else:
        return echo_response(204, 'item not exist')


def like_item(userkey, item_id):
    # check userkey
    if not is_userkey_valid(userkey) or userkey is None:
        return echo_response(401, 'missing user key')

    try:
        user = User.query.filter_by(api_key=userkey).first()

        item = Item.query.get(int(item_id))
        like_user = User.query.filter(User.liked_item.any(id=item_id)).all()
        liked_item = user.liked_item


        if user in like_user:
            liked_item.remove(item)
        else:
            liked_item.append(item)
        user.liked_item = liked_item
        db.session.commit()

        obj = get_item_json(item)
        result = {"status": 200, 'message': 'success', 'item': obj}
        return result
    except InvalidUsage:
        return echo_response(400, 'failed')


def bookmark_item(userkey, item_id):
    # check userkey
    if not is_userkey_valid(userkey) or userkey is None:
        return echo_response(401, 'missing user key')

    try:
        user = User.query.filter_by(api_key=userkey).first()
        item = Item.query.get(int(item_id))
        bookmark_user = User.query.filter(User.bookmarked_item.any(id=item.id)).all()
        bookmarked_item = user.bookmarked_item

        if user in bookmark_user:
            bookmarked_item.remove(item)
        else:
            bookmarked_item.append(item)
        user.bookmarked_item = bookmarked_item
        db.session.commit()

        obj = get_item_json(item)
        result = {"status": 200, 'message': 'success', 'item': obj}
        return result
    except InvalidUsage:
        return echo_response(400, 'failed')


def get_item_json(entity):
    if entity.photo is not None:
        photo = SERVER_URL + entity.photo
    else:
        photo = ''

    resulted_tags = []
    tags = entity.tags
    for tag in tags:
        resulted_tags.append({'id': tag.id, 'name': tag.name})

    business = Business.query.filter_by(id=entity.business_id).first()

    obj_business = {
        'id': business.id,
        'name': business.business_name,
        'address': business.address,
        'logo': SERVER_URL + (business.avatar or '')
    }

    category = Category.query.filter_by(id=entity.category_id).first()

    obj_category = {
        'id': category.id,
        'name': category.name
    }
    is_bookmarked = False
    is_liked = False

    bookmark_user = User.query.filter(User.bookmarked_item.any(id=entity.id)).all()
    like_user = User.query.filter(User.liked_item.any(id=entity.id)).all()

    tags = []
    entity_tags = entity.tags
    for entity_tag in entity_tags:
        tags.append(entity_tag.name)

    if len(bookmark_user) > 0:
        is_bookmarked = True
    if len(like_user) > 0:
        is_liked = True

    item = {
        'id': entity.id,
        'description': entity.description or '',
        'photo': photo,
        'category': obj_category or '{}',
        'type': entity.type or '',
        'business': obj_business,
        'is_liked': is_liked,
        'like_count': len(like_user),
        'is_bookmarked': is_bookmarked,
        'bookmark_count': len(bookmark_user),
        'tags': tags
    }
    return item


def is_userkey_valid(userkey):
    new_user = User.query.filter_by(api_key=userkey).first()
    return new_user
