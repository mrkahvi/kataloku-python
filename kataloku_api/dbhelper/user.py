from flask import request, jsonify

from kataloku_api import app
from kataloku_api.models import User, Item, Business, NearbyLog, SearchLog, NavigationLog, ItemViewLog, ItemListViewLog, BusinessViewLog, db
from kataloku_api.dbhelper.business import get_business_json_by_owner
from kataloku_api.utils.exceptions import InvalidUsage
from kataloku_api.utils.rest_handler import request_failed
from kataloku_api.utils.utilities import verify_post_params, echo_response, enum
from kataloku_api.configs.rest_config import BASE_URL
from datetime import datetime, date, timedelta
from werkzeug.utils import secure_filename
import os
import pprint
from config import SERVER_URL, AVATAR_UPLOAD_FOLDER, AVATAR_BASE_DIR

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

app.config['AVATAR_UPLOAD_FOLDER'] = AVATAR_UPLOAD_FOLDER

__RESOURCE = 'user'

import hashlib
import random

#REGISTER
def register_user():
    register_type = enum(EMAIL=1, FACEBOOK=2, TWITTER='3')

    any_missing = verify_post_params(['email', 'username', 'first_name', 'password'])

    #check required params
    if any_missing is not None:
        return any_missing

    #register type
    type = request.form.get('type', '1')

    username = request.form.get('username', '')
    first_name = request.form.get('first_name', '')
    last_name = request.form.get('last_name', '')
    email = request.form.get('email', '')
    password = request.form.get('password', '')
    city = request.form.get('city', '')
    push_id = request.form.get('city', '')
    social_id = request.form.get('social_id', '')
    social_token = request.form.get('social_token', '')
    secret_key = hashlib.sha224( str(random.getrandbits(256)) ).hexdigest()

    print 'secret-key:' + secret_key

    file_pic = None
    if 'picture' in request.files:
        print 'file:', request.files
        file_pic = request.files['picture']

    #check is username or email exists
    if is_username_exist(username):
        return echo_response(204, 'username exist. choose others')
    elif is_email_exist(email):
        return echo_response(204, 'email exist. choose others')
    else:
        try:
            new_user = User()
            new_user.username = username
            new_user.api_key = secret_key
            new_user.first_name = first_name
            new_user.last_name = last_name
            new_user.email = email
            new_user.password = password
            new_user.city = city
            new_user.push_id = push_id
            new_user.first_use = 1
            new_user.is_active = 0

            #print 'type:'+ type
            if type == register_type.FACEBOOK:
                new_user.fb_id = social_id
                new_user.fb_token = social_token
            elif type == register_type.TWITTER:
                new_user.twitter_id = social_id
                new_user.twitter_token = social_token

            if file_pic is not None:
                filename = secure_filename(file_pic.filename)
                file_pic.save(os.path.join(app.config['AVATAR_UPLOAD_FOLDER'], filename))
                new_user.avatar = os.path.join(AVATAR_BASE_DIR, filename)

            db.session.add(new_user)
            db.session.commit()

            user = get_user_json(new_user)
            obj = {
                'status': 200,
                'message': 'success',
                'user': user
            }
            return obj

        except InvalidUsage:
            return echo_response(400, 'failed')


#LOGIN USING USERNAME
# (BASE_URL + __RESOURCE + '/login', methods=['POST'])
def login():
    any_missing = verify_post_params(['username', 'password'])

    #check required params
    if any_missing is not None:
        return any_missing

    username = request.form.get('username')
    password = request.form.get('password')
    push_id = request.form.get('push_id', '')

    #entity = User.query.filter_by(username=username, password=password).first()
    entity = User.query.filter_by(username=username).first()

    user = {}

    if entity is not None:
        if password != entity.password:
            response_code = 204
            message = "password invalid"
        else:
            # try:
            if push_id is not None:
                #update GCM push id
                entity.push_id = push_id
                db.session.add(entity)
                db.session.commit()

            print 'exist'
            response_code = 200
            message = 'success'
            user = get_user_json(entity)

            # except:
            #     response_code = 204
            #     message = 'failed'
    else:
        response_code = 204
        message = "user doesn't exist"

    obj = {
        'status': response_code,
        'message': message,
        'user': user
    }
    return obj


# (BASE_URL + __RESOURCE +'/social_check', methods=['POST'])
def social_auth_check():
    any_missing = verify_post_params(['social_id', 'type'])

    #check required params
    if any_missing is not None:
        return any_missing

    social_type = enum(FACEBOOK=1, TWITTER='2')

    # if request.method == 'POST':
    social_id = request.form.get('social_id', '')
    push_id = request.form.get('push_id', '')
    sc_type = request.form.get('type', '')
    print social_id

    entity = None
    if sc_type == social_type.FACEBOOK:
        entity = User.query.filter_by(fb_id=social_id).first()
        print('facebook')
    elif sc_type == social_type.TWITTER:
        entity = User.query.filter_by(twitter_id=social_id).first()
        print('twitter')

    if entity is None:
        response_code = 204
        message = 'empty'
        user = {}
    else:
        entity.push_id = push_id
        db.session.add(entity)
        db.session.commit()

        response_code = 200
        message = 'exist'
        user = get_user_json(entity)

    obj = {
        'responseCode': response_code,
        'message': message,
        'user': user
    }
    return obj


#GET USER
# (BASE_URL + __RESOURCE + '/<userkey>/<user_id>', methods=['GET'])
def get_user(userkey, user_id):
    print user_id
    if user_id is None:
        return echo_response(401, 'missing user id')
    if userkey is None:
        return echo_response(401, 'missing user key')

    entity = User.query.filter_by(id=user_id).first()

    if entity is None:
        return echo_response(204, 'user not found')
    else:
        user = get_user_json(entity)
        return {'status': 200, 'message': 'success', 'user': user}


#UPDATE PROFILE
# (BASE_URL + __RESOURCE + '/profile_update/<userkey>', methods=['POST'])
def update_profiles(userkey):
    #check userkey
    if not is_userkey_valid(userkey) or userkey is None:
        return echo_response(401, 'missing user key')

    any_missing = verify_post_params(['user_id'])

    #check required params
    if any_missing is not None:
        return any_missing

    #check user
    user_id = request.form.get('user_id')
    new_user = User()
    new_user = User.query.filter_by(id=user_id).first()
    print user_id
    if new_user is None:
        return echo_response(204, 'user not found')


    #--profile udpate
    first_name = request.form.get('first_name')
    last_name = request.form.get('last_name')
    birthday = request.form.get('birthday')
    city = request.form.get('city')
    address = request.form.get('address')
    postal_code = request.form.get('postal_code')
    phone = request.form.get('phone')
    profession = request.form.get('profession')
    province = request.form.get('province')
    district = request.form.get('district')

    file_pic = None
    if 'picture' in request.files:
        print 'file:', request.files
        file_pic = request.files['picture']
    try:
        if not (first_name is None): new_user.first_name = first_name
        if not (last_name is None): new_user.last_name = last_name
        if not (birthday is None): new_user.birthday = birthday
        if not (city is None): new_user.city = city
        if not (address is None): new_user.address = address
        if not (postal_code is None): new_user.postal_code = postal_code
        if not (phone is None): new_user.phone = phone
        if not (profession is None): new_user.profession = profession
        #if not (province is None): new_user.province = province
        #if not (district is None): new_user.district = district

        if file_pic is not None:
            filename = secure_filename(file_pic.filename)
            file_pic.save(os.path.join(app.config['AVATAR_UPLOAD_FOLDER'], filename))
            new_user.avatar = os.path.join(AVATAR_BASE_DIR, filename)

        db.session.add(new_user)
        db.session.commit()
        user = get_user_json(new_user)
        obj = {
            'status': 200,
            'message': 'success',
            'user': user
        }
        return obj

    except InvalidUsage:
        return echo_response(400, 'failed')
    # except:
    #     return request_failed()


def get_user_json(entity):
    business = get_business_json_by_owner(entity.id)


    if entity.avatar:
       avatar = str(SERVER_URL)+entity.avatar
    else:
        avatar = ''

    user = {
        'id': entity.id,
        'username': entity.username or '',
        'first_name': entity.first_name or '',
        'last_name': entity.last_name or '',
        'avatar': avatar,
        'gender': entity.gender or 1,
        'birthday': str(entity.birthday) or '',
        'profession': entity.profession or '',
        'email': entity.email or '',
        'city' : entity.city or '',
        'api_key': entity.api_key or '',
        'phone': entity.phone or '',
        'address': entity.address or '',
        'postal_code': entity.postal_code or '',
        'fb_id': entity.fb_id or '',
        'fb_token': entity.fb_token or '',
        'twitter_id': entity.twitter_id or '',
        'twitter_token': entity.twitter_token or '',
        'twitter_secret': entity.twitter_secret or '',
        'is_active': entity.is_active,
        'first_use': entity.first_use,
        'business': business
    }
    return user


def like_item(userkey, item_id):
    if not is_userkey_valid(userkey) or userkey is None:
        return echo_response(401, 'missing user key')

    try:
        item = Item.query.get(int(item_id))
        user = User.query.filter_by(api_key=userkey).first()
        all_liked_item = user.liked_item
        pprint.pprint(all_liked_item)
        liked_item = Item.query.filter(Item.like_user.any(id=item_id)).first()
        if liked_item:
            all_liked_item.remove(liked_item)
        else:
            all_liked_item.append(liked_item)
            
        pprint.pprint(all_liked_item)
        user.liked_item = all_liked_item
        db.session.commit()

        obj = get_user_json(user)
        result = {"status": 200, 'message': 'success', 'user': obj}
        return result
    except InvalidUsage:
        return echo_response(400, 'failed')

def unlike_item(userkey, item_id):
     #check userkey
    if not is_userkey_valid(userkey) or userkey is None:
        return echo_response(401, 'missing user key')

    try:
        user = User.query.filter_by(api_key=userkey).first()
        item = Item.query.get(int(item_id))
        liked_item = user.liked_item
        user.liked_item = liked_item
        db.session.commit()

        obj = get_user_json(user)
        result = {"status": 200, 'message': 'success', 'user': obj}
        return result
    except InvalidUsage:
        return echo_response(400, 'failed')

def bookmark_item(userkey, item_id):
     #check userkey
    if not is_userkey_valid(userkey) or userkey is None:
        return echo_response(401, 'missing user key')

    try:
        user = User.query.filter_by(api_key=userkey).first()
        item = Item.query.get(int(item_id))
        bookmarked_item = user.bookmarked_item
        bookmarked_item.append(item)
        user.bookmarked_item = bookmarked_item
        db.session.commit()

        obj = get_user_json(user)
        result = {"status": 200, 'message': 'success', 'user': obj}
        return result
    except InvalidUsage:
        return echo_response(400, 'failed')

def dashboard_current_count_stat(userkey):
    user = is_userkey_valid(userkey)
    if not user or userkey is None:
        return echo_response(401, 'missing user key')

    today = date.today()
    tomorrow = today + timedelta(days=1)
    pprint.pprint(today)
    pprint.pprint(user.business.id)

    nearby = NearbyLog.query.filter(NearbyLog.date >= today, NearbyLog.date < tomorrow, NearbyLog.business_id == user.business.id).count()
    search = SearchLog.query.filter(SearchLog.date >= today, SearchLog.date < tomorrow, SearchLog.business_id == user.business.id).count()
    business_view = BusinessViewLog.query.filter(BusinessViewLog.date >= today, BusinessViewLog.date < tomorrow, BusinessViewLog.business_id == user.business.id).count()
    item_list_view = ItemListViewLog.query.filter_by(date=today, business_id=user.business.id).count()
    # itemView = ItemViewLog.query.filter(ItemViewLog.date >= today, ItemViewLog.date < tomorrow, ItemViewLog.user_id == businessUserId).count()
    item_view = db.session.query(ItemViewLog).join(Item).filter(ItemViewLog.date >= today, ItemViewLog.date < tomorrow, Item.business_id == user.business.id).count()
    navigation = NavigationLog.query.filter(NavigationLog.date >= today, NavigationLog.date < tomorrow, NavigationLog.business_id == user.business.id).count()

    logs = {}
    logs['nearbies'] = nearby
    logs['searches'] = search
    logs['business_views'] = business_view
    logs['item_browses'] = item_list_view
    logs['item_views'] = item_view
    logs['navigations'] = navigation

    result = {"status": 200,  "results": logs}
    return result





def is_username_exist(username):

    entity = User.query.filter_by(username=username).first()
    if entity is None:
        return False
    else:
       return True


def is_email_exist(email):
    entity = User.query.filter_by(email=email).first()
    if entity is None:
        return False
    else:
        return True


def is_userkey_valid(userkey):
    new_user = User.query.filter_by(api_key=userkey).first()
    return new_user



#
# from functools import wraps
# from flask import request, Response
#
#
# def authenticate():
#     """Sends a 401 response that enables basic auth"""
#     return Response(
#     'Could not verify your access level for that URL.\n'
#     'You have to login with proper credentials', 401,
#     {'WWW-Authenticate': 'Basic realm="Login Required"'})
#
# def requires_auth(f):
#     @wraps(f)
#     def decorated(*args, **kwargs):
#         auth = request.authorization
#         if not auth or not check_auth(auth.username, auth.password):
#             return authenticate()
#         return f(*args, **kwargs)
#     return decorated
#
# def check_auth(username, password):
#     """This function is called to check if a username /
#     password combination is valid.
#     """
#     return username == 'admin' and password == 'secret'
#
#
# # ('/secret-page')
# @requires_auth
# def secret_page():
#     return 'rahasia!'