__author__ = 'ikhsanudinhakim'

from kataloku_api.models import Item
from flask import request, jsonify
from kataloku_api.utils.exceptions import InvalidUsage
from kataloku_api import app
from kataloku_api.models import User, db, Offering
from kataloku_api.configs.rest_config import BASE_URL
from werkzeug.utils import secure_filename
from kataloku_api.configs import global_const
import os
from kataloku_api.utils.utilities import verify_post_params, echo_response, enum
from kataloku_api.utils.utilities import verify_post_params, echo_response
from config import SERVER_URL, ITEM_UPLOAD_FOLDER, ITEM_BASE_DIR
from datetime import datetime, date, timedelta

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

app.config['ITEM_UPLOAD_FOLDER'] = ITEM_UPLOAD_FOLDER

__RESOURCE = 'offering'

offering_type = enum(NEW=1, DISCOUNT=2, SPECIAL_OFFER='3')


# (BASE_URL + __RESOURCE + '/item/list/<userkey>/<business_id>/<type>', methods=['GET'])
def get_item_for_offering(userkey, business_id, type):
    user = is_userkey_valid(userkey)
    if not user or userkey is None:
        return echo_response(401, 'missing user key')

    lim = request.args.get('limit', global_const.DEFAULT_RETURN_COUNT)
    off = request.args.get('offset')

    if type == 1:
        items = Item.query.filter_by(user_id=business_id).order_by(Item.id.desc()).limit(lim).all()
    else:
        query = 'SELECT * FROM Item WHERE business_id=' + business_id + ' AND price > 0'
        items = Item.query.from_statement(query).all()

    json_results = []
    if items:
        for result in items:
            if result.photo is not None:
                photo = SERVER_URL + result.photo
            else:
                photo = ''

            d = {'id': result.id,
                 'name': result.name,
                 'price': result.price,
                 'description': result.description,
                 'category_id': result.category_id,
                 'business_id': result.business_id,
                 'unit': result.unit,
                 'photo': photo,
                 'type': result.type
                 }
            json_results.append(d)
        obj = {'status': 200, 'message': 'success', 'items': json_results}
        return jsonify(obj)
    else:
        return echo_response(204, 'There are no item yet.')


# (BASE_URL + __RESOURCE + '/create/<userkey>', methods=['POST'])
def make_offering(userkey):
    user = is_userkey_valid(userkey)
    if not user or userkey is None:
        return echo_response(401, 'missing user key')

    any_missing = verify_post_params(['item_id', 'type', 'message', 'end'])

    # check required params
    if any_missing is not None:
        return any_missing

    item_id = request.form.get('item_id')
    type = request.form.get('type')
    message = request.form.get('message')
    end = request.form.get('end')
    end_nums = end.split(' ')
    date = end_nums[0]
    time = end_nums[1]

    date_num = date.split('-')
    time_num = time.split(':')

    item = Item.query.get(item_id)

    try:
        entity = Offering()
        entity.type = type
        entity.item = item
        entity.message = message
        entity.start = datetime.now()
        entity.end = datetime(int(date_num[0]), int(date_num[1]), int(date_num[2]), int(time_num[0]), int(time_num[1]))
        db.session.add(entity)
        db.session.commit()

        obj = {
            'id': entity.id,
            'item_id': entity.item.id,
            'message': entity.message,
            'end': entity.end.strftime('%Y-%m-%d %H:%M:%S')
        }

        result = {"status": 200,  "message": message, "offering": obj}

        return result
    except InvalidUsage:
        result = {"status": 400, "message": message}
        return result


def offering_nearby(userkey, distance, lat, lng, page):
    user = is_userkey_valid(userkey)
    if (not user or userkey is None) and userkey != '000000000':
        return echo_response(401, 'missing user key')

    # end = 20 * int(page)
    # start = int(end) - 20

    end = 20 * (int(page) + 1)
    start = int(end) - 20

    current = datetime.now()
    current = current.strftime('%Y-%m-%d %H:%M:%S')

    # query = "SELECT Item.id AS item_id, Offering.id AS offering_id, Offering.type AS type, Offering.message AS message, Offering.end AS end, Item.photo AS photo, " \
    #         "Business.id AS user_id, Business.avatar AS business_picture, Business.business_name AS business_name, address, longitude, latitude, ( 6371 * acos( cos( radians(" + lat + " ) ) * cos( radians(Business.latitude) ) * cos( radians( Business.longitude ) - radians(" + lng + ") ) + sin( radians(" + lat + " ) ) * sin(radians(Business.latitude)) ) ) AS distance FROM Offering LEFT JOIN Item ON Offering.item_id = Item.id LEFT JOIN Business ON Item.business_id=Business.id WHERE end > '" + current + "' HAVING distance <= " + distance + " OR distance = 0 ORDER BY distance LIMIT " + str(start) + " , " + str(end)

    query = "SELECT Item.id AS item_id, Offering.id AS offering_id, Offering.type AS type, Offering.message AS message, Offering.end AS end, Item.photo AS photo, " \
            "Business.id AS user_id, Business.avatar AS business_picture, Business.business_name AS business_name, address, longitude, latitude, ( 6371 * acos( cos( radians(" + lat + " ) ) * cos( radians(Business.latitude) ) * cos( radians( Business.longitude ) - radians(" + lng + ") ) + sin( radians(" + lat + " ) ) * sin(radians(Business.latitude)) ) ) AS distance FROM Offering LEFT JOIN Item ON Offering.item_id = Item.id LEFT JOIN Business ON Item.business_id=Business.id WHERE end > '" + current + "' HAVING distance <= " + distance + " OR distance = 0 ORDER BY distance LIMIT " + str(start) + " , " + str(end)


    results = db.engine.execute(query)
    # cur = dbCon.cursor(MySQLdb.cursors.DictCursor)
    # cur.execute(query)
    # rows = cur.fetchall()

    items = []

    i = 0
    for row in results:

        current_item = Item.query.filter_by(id=row['item_id']).first()

        tags = []
        entity_tags = current_item.tags
        for entity_tag in entity_tags:
            tags.append(entity_tag.name)

        item = {}
        item['offering_id'] = row['offering_id']
        item['type'] = row['type']
        item['message'] = row['message']
        item['end'] = row['end'].strftime('%Y-%m-%d %H:%M:%S')
        item['item_id'] = row['item_id']
        item['tags'] = tags
        item['user_id'] = row['user_id']
        item['business_name'] = row['business_name']
        item['business_picture'] = SERVER_URL+row['business_picture']
        lat = row['latitude']
        lng = row['longitude']
        distance = row['distance']
        item['address'] = row['address']
        item['photo'] = str(SERVER_URL) + row['photo']
        item['lat'] = lat
        item['lng'] = lng
        # item['tags'] = tag_array
        item['distance'] = distance
        items.append(item)
        i = i + 1


    if len(items) > 0:
        obj = {'status': 200, 'message': 'message', 'items': items}
        return obj
    else:
        obj = {'status': 204, 'message': 'there are no nearby offering', 'items': []}
        return obj


def detail_offering(item_id):
    offering = Offering.query.filter_by(item_id=item_id).first()
    if offering is not None:
        json_offering = {
            'id': offering.id,
            'type': offering.type,
            'message': offering.message,
            'end': offering.end
        }
    else:
        json_offering = None

    return json_offering


def is_userkey_valid(userkey):
    new_user = User.query.filter_by(api_key=userkey).first()
    return new_user
