from kataloku_api import app
from kataloku_api.models import db,User, Review
from flask import request, jsonify
from kataloku_api.utils.utilities import verify_post_params, echo_response, enum
from kataloku_api.utils.exceptions import InvalidUsage
from kataloku_api.utils.utilities import echo_response
from config import SERVER_URL,AVATAR_UPLOAD_FOLDER, SHOWCASE_UPLOAD_FOLDER
from kataloku_api.configs.rest_config import BASE_URL

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
app.config['UPLOAD_FOLDER'] = AVATAR_UPLOAD_FOLDER
app.config['SHOWCASE_UPLOAD_FOLDER'] = SHOWCASE_UPLOAD_FOLDER

__RESOURCE = 'review'


#REGISTERs
def add_review(userkey):
    #check userkey
    if not is_userkey_valid(userkey) or userkey is None:
        return echo_response(401, 'missing user key')

    any_missing = verify_post_params(['business_id', 'rate', 'message', 'user_id'])

    #check required params
    if any_missing is not None:
        return any_missing

    #register type
    business_id = request.form.get('business_id', '')
    user_id = request.form.get('user_id', '')
    message = request.form.get('message', '')
    rate = request.form.get('rate', '')

    try:
        new_review = Review()
        new_review.user_id = user_id
        new_review.business_id = business_id
        new_review.message = message
        new_review.rating = rate

        db.session.add(new_review)
        db.session.commit()

        reviews = get_business_review(business_id, 10)

        #user = get_user_json(new_review.user)
        # review = {
        #     'user': user,
        #     'message': new_review.message,
        #     'rate':  new_review.rating,
        #     'time':  new_review.time
        # }
        obj = {
            'status': 200,
            'message': 'success',
            'reviews': reviews
        }
        return obj

    except InvalidUsage:
        return echo_response(400, 'failed')


def get_all_business_review(userkey, business_id):
    user = is_userkey_valid(userkey)
    if not user or userkey is None:
        return echo_response(401, 'missing user key')

    json_results = []

    results = Review.query.filter_by(business_id=business_id).order_by(Review.id.desc()).all()
    if results:
        for result in results:
            reviewer = User.query.filter_by(id=result.user_id).first()
            if reviewer.avatar:
               avatar = str(SERVER_URL)+reviewer.avatar
            else:
                avatar = ''

            user = {
                'id': reviewer.id,
                'username': reviewer.username or '',
                'first_name': reviewer.first_name or '',
                'last_name': reviewer.last_name or '',
                'avatar': avatar,
                'gender': reviewer.gender or '',
                'birthday': str(reviewer.birthday) or '',
                'profession': reviewer.profession or '',
                'email': reviewer.email or '',
                'api_key': reviewer.api_key or '',
                'phone': reviewer.phone or '',
                'address': reviewer.address or '',
                'postal_code': reviewer.postal_code or '',
                'fb_id': reviewer.fb_id or '',
                'fb_token': reviewer.fb_token or '',
                'twitter_id': reviewer.twitter_id or '',
                'twitter_token': reviewer.twitter_token or '',
                'twitter_secret': reviewer.twitter_secret or '',
                'first_use': reviewer.first_use
            }

            d = {'id': result.id,
                'message': result.message,
                'rating': result.rating,
                'time': result.time,
                'user': user,
                'business_id': result.business_id
            }
            json_results.append(d)
        obj = {'status': 200, 'message': 'success', 'reviews': json_results}

        return obj
    else:
        return echo_response(204, 'There are no reviews yet.');



def get_detail_review(user_id, business_id):
    d = None
    result = Review.query.filter_by(business_id=business_id, user_id=user_id).first()
    if result is not None:
        d = {'id': result.id,
            'message': result.message,
            'rating': result.rating,
            'time': result.time
        }
    return d


#get last 3 reviews
def get_business_review(business_id, _limit):

    json_results = []
    results = Review.query.filter_by(business_id=business_id).order_by(Review.id.desc()).limit(_limit).all()
    res_count = Review.query.filter_by(business_id=business_id).order_by(Review.id.desc()).limit(_limit).count()
    print 'count:' + str(res_count) + str(business_id)
    if results is not None:
        for result in results:
            reviewer = User.query.filter_by(id=result.user_id).first()
            if reviewer.avatar:
               avatar = str(SERVER_URL)+reviewer.avatar
            else:
                avatar = ''

            user = {
                'id': reviewer.id,
                'username': reviewer.username or '',
                'first_name': reviewer.first_name or '',
                'last_name': reviewer.last_name or '',
                'avatar': avatar,
                'gender': reviewer.gender or '',
                'birthday': str(reviewer.birthday) or '',
                'profession': reviewer.profession or '',
                'email': reviewer.email or '',
                'api_key': reviewer.api_key or '',
                'phone': reviewer.phone or '',
                'address': reviewer.address or '',
                'postal_code': reviewer.postal_code or '',
                'fb_id': reviewer.fb_id or '',
                'fb_token': reviewer.fb_token or '',
                'twitter_id': reviewer.twitter_id or '',
                'twitter_token': reviewer.twitter_token or '',
                'twitter_secret': reviewer.twitter_secret or '',
                'first_use': reviewer.first_use
            }

            d = {'id': result.id,
                'message': result.message,
                'rating': result.rating,
                'time': result.time,
                'user': user,
                'business_id': result.business_id
                }
            json_results.append(d)

    return  json_results



def check_user_has_reviewed(user_id, business_id):
    has_reviewed = Review.query.filter_by(business_id=business_id, user_id=user_id).first()
    return has_reviewed is not None

def is_userkey_valid(userkey):
    new_user = User.query.filter_by(api_key=userkey).first()
    return new_user

def get_user_json(entity):

    if entity.avatar:
       avatar = str(SERVER_URL)+entity.avatar
    else:
        avatar = ''

    user = {
        'id': entity.id,
        'username': entity.username or '',
        'first_name': entity.first_name or '',
        'last_name': entity.last_name or '',
        'avatar': avatar,
        'gender': entity.gender or '',
        'birthday': str(entity.birthday) or '',
        'profession': entity.profession or '',
        'email': entity.email or '',
        'api_key': entity.api_key or '',
        'phone': entity.phone or '',
        'address': entity.address or '',
        'postal_code': entity.postal_code or '',
        'fb_id': entity.fb_id or '',
        'fb_token': entity.fb_token or '',
        'twitter_id': entity.twitter_id or '',
        'twitter_token': entity.twitter_token or '',
        'twitter_secret': entity.twitter_secret or '',
        'is_active': entity.is_active,
        'first_use': entity.first_use
    }
    return user