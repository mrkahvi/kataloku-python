from kataloku_api import app
from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, Float
from sqlalchemy.orm import relationship, backref
from datetime import datetime
import uuid
import pprint

# app = Flask(__name__)
db = SQLAlchemy(app)

business_tags = db.Table(
    'Business_Tag',
    Column('business_id', Integer, ForeignKey('Business.id')),
    Column('tag_id', Integer, ForeignKey('BusinessTag.id'))
)

item_tags = db.Table(
    'Item_Tag',
    Column('item_id', Integer, ForeignKey('Item.id')),
    Column('tag_id', Integer, ForeignKey('ItemTag.id'))
)

item_bookmark = db.Table(
    'BookmarkedItem',
    Column('item_id', Integer, ForeignKey('Item.id')),
    Column('user_id', Integer, ForeignKey('User.id'))
)

business_bookmark = db.Table(
    'BookmarkedBusiness',
    Column('business_id', Integer, ForeignKey('Business.id')),
    Column('user_id', Integer, ForeignKey('User.id'))
)

item_like = db.Table(
    'LikedItem',
    Column('item_id', Integer, ForeignKey('Item.id')),
    Column('user_id', Integer, ForeignKey('User.id'))
)

class Item(db.Model):
    __tablename__ = 'Item'
    id = Column(Integer, primary_key=True)
    description = Column(String(255))
    category_id = Column(Integer, ForeignKey('Category.id'))
    business_id = Column(Integer, ForeignKey('Business.id'))
    photo = Column(String(255))
    type = Column(String(255))
    tags = relationship('ItemTag', secondary=item_tags, backref=backref('items', lazy='dynamic'))
    offerings = relationship('Offering', backref='item', lazy='dynamic')


class ItemViewLog(db.Model):
    __tablename__ = 'ItemViewLog'
    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    item_id = Column(Integer, ForeignKey('Item.id'))
    item = relationship("Item", backref='incoming_item_view_logs', foreign_keys=[item_id])
    user_id = Column(Integer, ForeignKey('User.id'))
    ref = Column(Integer)


class Category(db.Model):
    __tablename__ = 'Category'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    slug = Column(String(255))
    description = Column(String(255))
    items = relationship('Item', backref='category', lazy='dynamic')


class User(db.Model):
    __tablename__ = 'User'
    id = Column(Integer, primary_key=True)
    username = Column(String(255))
    create_date = Column(DateTime, default=datetime.utcnow())
    api_key = Column(String(255))
    first_name = Column(String(255))
    last_name = Column(String(255))
    address = Column(String(255))
    gender = Column(String(1))
    birthday = Column(String(255))
    profession = Column(String(255))
    city = Column(String(255))
    password = Column(String(255))
    email = Column(String(255))
    fb_id = Column(String(255))
    fb_token = Column(String(255))
    twitter_id = Column(String(255))
    #twitter_email = Column(String(255))
    twitter_token = Column(String(255))
    twitter_secret = Column(String(255))
    push_id = Column(String(255))
    postal_code = Column(String(255))
    avatar = Column(String(255))
    phone = Column(String(255))
    balances = Column(Integer)
    first_use = Column(Integer)
    is_active = Column(Integer)
    business = relationship('Business', backref='user', uselist=False)
    bookmarked_item = relationship('Item', secondary=item_bookmark, backref=backref('bookmark_user', lazy='dynamic'))
    liked_item = relationship('Item', secondary=item_like, backref=backref('like_user', lazy='dynamic'))
    bookmarked_business = relationship('Business', secondary=business_bookmark, backref=backref('business_bookmark_user', lazy='dynamic'))

    def __init__(self):
        self.api_key = uuid.uuid4().hex


class Business(db.Model):
    __tablename__ = 'Business'
    id = Column(Integer, primary_key=True)
    slug = Column(String(255))
    create_date = Column(DateTime, default=datetime.utcnow())
    business_name = Column(String(255))
    email = Column(String(255))
    address = Column(String(255))
    description = Column(String(255))
    rating = Column(Integer)
    is_open = Column(Integer)
    first_use = Column(Integer)
    province = Column(String(255))
    city = Column(String(255))
    district = Column(String(255))
    village = Column(String(255))
    postal_code = Column(String(255))
    avatar = Column(String(255))
    latitude = Column(String(255))
    longitude = Column(String(255))
    phone = Column(String(255))
    website = Column(String(255))
    operation_days = Column(String(500))
    items = relationship('Item', backref='business', lazy='dynamic')
    pictures = relationship('Picture', backref='business', lazy='dynamic')
    category_id = Column(Integer, ForeignKey('BusinessCategory.id'))
    tags = relationship('BusinessTag', secondary=business_tags, backref=backref('businesses', lazy='dynamic'))
    user_id = Column(Integer, ForeignKey('User.id'))
    # user = relationship('User', backref='business', foreign_keys=user_id)


class Review(db.Model):
    __tablename__ = 'Review'
    id = Column(Integer, primary_key=True)
    message = Column(String(255))
    rating = Column(Integer)
    time = Column(DateTime, default=datetime.utcnow())
    user_id = Column(Integer, ForeignKey('User.id'), nullable=True)
    business_id = Column(Integer, ForeignKey('Business.id'), nullable=True)
    user = relationship('User', backref='user_reviews', foreign_keys=user_id)
    business = relationship("Business", backref='business_reviews', foreign_keys=[business_id])


class BusinessCategory(db.Model):
    __tablename__ = 'BusinessCategory'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    slug = Column(String(255))
    description = Column(String(255))
    type_id = Column(String(255))

class Type(db.Model):
    __tablename__ = 'Type'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    type_id = Column(String(255))

class Picture(db.Model):
    __tablename__ = 'Picture'
    id = Column(Integer, primary_key=True)
    picture = Column(String(255))
    business_id = Column(Integer, ForeignKey('Business.id'))


class Offering(db.Model):
    __tablename__ = 'Offering'
    id = Column(Integer, primary_key=True)
    type = Column(String(255))
    item_id = Column(Integer, ForeignKey('Item.id'))
    message = Column(String(255))
    start = Column(DateTime)
    end = Column(DateTime)


class BusinessTag(db.Model):
    __tablename__ = 'BusinessTag'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))


class ItemTag(db.Model):
    __tablename__ = 'ItemTag'
    id = Column(Integer, primary_key=True)
    name = Column(String(255), unique=True)


class SearchLog(db.Model):
    __tablename__ = 'SearchLog'
    id = Column(Integer, primary_key=True)
    date = Column(DateTime, default=datetime.utcnow())
    user_id = Column(Integer, ForeignKey('User.id'))
    business_id = Column(Integer, ForeignKey('Business.id'))
    user = relationship('User', backref='user_search_log', foreign_keys=[user_id])
    business = relationship('Business', backref='business_search_log', foreign_keys=[business_id])
    keyword = Column(String(255))
    distance = Column(Float)
    order_id = Column(Integer)


class NearbyLog(db.Model):
    __tablename__ = 'NearbyLog'
    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    user_id = Column(Integer, ForeignKey('User.id'))
    business_id = Column(Integer, ForeignKey('Business.id'))
    user = relationship('User', backref='user_nearby_log', foreign_keys=[user_id])
    business = relationship('Business', backref='business_user_nearby_log', foreign_keys=[business_id])
    distance = Column(Float)
    order_id = Column(Integer)


class BusinessViewLog(db.Model):
    __tablename__ = 'BusinessViewLog'
    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    user_id = Column(Integer, ForeignKey('User.id'))
    business_id = Column(Integer, ForeignKey('Business.id'))
    user = relationship('User', backref='user_business_view_log', foreign_keys=[user_id])
    business = relationship('Business', backref='business_user_business_view_log', foreign_keys=[business_id])
    ref = Column(Integer)


class ItemListViewLog(db.Model):
    __tablename__ = 'ItemListViewLog'
    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    user_id = Column(Integer, ForeignKey('User.id'))
    business_id = Column(Integer, ForeignKey('Business.id'))
    user = relationship('User', backref='user_item_list_view_log', foreign_keys=[user_id])
    business = relationship('Business', backref='business_user_item_list_view_log', foreign_keys=[business_id])
    ref = Column(Integer)


class NavigationLog(db.Model):
    __tablename__ = 'NavigationLog'
    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    user_id = Column(Integer, ForeignKey('User.id'))
    business_id = Column(Integer, ForeignKey('Business.id'))
    user = relationship('User', backref='user_navigation_log', foreign_keys=[user_id])
    business = relationship('Business', backref='business_user_navigation_log', foreign_keys=[business_id])
    distance = Column(Float)

