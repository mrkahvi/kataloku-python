from kataloku_api import app
from kataloku_api.models import db,User, Review
from flask import request, jsonify
from kataloku_api.utils.utilities import verify_post_params, echo_response, enum
from kataloku_api.utils.exceptions import InvalidUsage
from kataloku_api.utils.utilities import echo_response
from kataloku_api.dbhelper import review
from config import SERVER_URL,AVATAR_UPLOAD_FOLDER, SHOWCASE_UPLOAD_FOLDER
from kataloku_api.configs.rest_config import BASE_URL

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
app.config['UPLOAD_FOLDER'] = AVATAR_UPLOAD_FOLDER
app.config['SHOWCASE_UPLOAD_FOLDER'] = SHOWCASE_UPLOAD_FOLDER

__RESOURCE = 'review'


#REGISTER
@app.route(BASE_URL + __RESOURCE + '/add/<userkey>', methods=['POST'])
def add_review(userkey):
    obj = review.add_review(userkey)
    return jsonify(obj)


@app.route(BASE_URL + __RESOURCE + '/<userkey>/<business_id>', methods=['GET'])
def get_business_review(userkey, business_id):
    obj = review.get_all_business_review(userkey, business_id)
    return jsonify(obj)


def get_detail_review(user_id, business_id):
    return review.get_detail_review(user_id, business_id)


#get last 3 reviews
def get_business_review(business_id, _limit):
    return review.get_business_review(business_id, _limit)


def check_user_has_reviewed(user_id, business_id):
    return review.check_user_has_reviewed(user_id, business_id)


def is_userkey_valid(userkey):
    return review.is_userkey_valid(userkey)


def get_user_json(entity):
    return review.get_user_json(entity)