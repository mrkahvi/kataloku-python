from kataloku_api.models import Item
from flask import request, jsonify
from datetime import datetime, date, timedelta
from kataloku_api.utils.exceptions import InvalidUsage
from kataloku_api import app
from kataloku_api.models import User,db, Category, SearchLog
from kataloku_api.configs.rest_config import BASE_URL
from kataloku_api.dbhelper import item
from werkzeug.utils import secure_filename
from kataloku_api.configs import global_const
import os
from kataloku_api.utils.utilities import verify_post_params, echo_response
from config import SERVER_URL, ITEM_UPLOAD_FOLDER, ITEM_BASE_DIR
from config import dbCon
import MySQLdb

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

app.config['ITEM_UPLOAD_FOLDER'] = ITEM_UPLOAD_FOLDER

__RESOURCE = 'item'


# Required APIs:
# 1. Add Item -ok
# 2. Get Item Categories -ok
# 3. Show Business Item -ok
# 4. Update Item -ok
# 5. Search Nearby - ok
# 6. Show Detail -ok


@app.route(BASE_URL + __RESOURCE + '/add/<userkey>', methods=['POST'])
def add_item(userkey):
    obj = item.add_item(userkey)
    return jsonify(obj)


@app.route(BASE_URL + __RESOURCE + '/update/<userkey>', methods=['POST'])
def update_item(userkey):
    result = item.update_item(userkey)
    return jsonify(result)


#GET ITEM CATEGORIES
@app.route(BASE_URL + __RESOURCE + '/categories', methods=['GET'])
def get_item_categories():
    obj = item.get_item_categories()
    return jsonify(obj)


@app.route(BASE_URL + __RESOURCE + '/list/<userkey>/<business_id>/<ref>', methods=['GET'])
def get_business_items(userkey, business_id, ref):
    obj = item.get_business_items(userkey, business_id, ref)
    return jsonify(obj)


@app.route(BASE_URL + __RESOURCE + '/search/nearby/<userkey>/<keyword>/<distance>/<lat>/<lng>/<page>', methods=['GET'])
def search_nearby(userkey, keyword, distance, lat, lng, page):
    result = item.search_nearby(userkey, keyword, distance, lat, lng, page)
    return jsonify(result)

@app.route(BASE_URL + __RESOURCE + '/like/<userkey>/<item_id>', methods=['GET'])
def like_item(userkey, item_id):
    obj = item.like_item(userkey, item_id)
    return jsonify(obj)

@app.route(BASE_URL + __RESOURCE + '/bookmark/<userkey>/<item_id>', methods=['GET'])
def bookmark_item(userkey, item_id):
    obj = item.bookmark_item(userkey, item_id)
    return jsonify(obj)


@app.route(BASE_URL + __RESOURCE + '/detail/<userkey>/<item_id>/<ref>', methods=['GET'])
def get_item_detail(userkey, item_id, ref):
    obj = item.get_item_detail(userkey, item_id, ref)
    return  jsonify(obj)

def get_item_json(entity):
    return item.get_item_json(entity)

def is_userkey_valid(userkey):
    return item.is_userkey_valid(userkey)
