from flask import request, jsonify, json
from slugify import slugify
from kataloku_api.utils.exceptions import InvalidUsage
from kataloku_api import app
from kataloku_api.utils.rest_handler import request_failed
from kataloku_api.models import db, Category, User, BusinessTag, Business, BusinessCategory, Picture, Item
from kataloku_api.configs.rest_config import BASE_URL
from kataloku_api.dbhelper import business, item, user, review
from review import get_business_review, check_user_has_reviewed, get_detail_review
from werkzeug.utils import secure_filename
import os
from kataloku_api.utils.utilities import verify_post_params, echo_response, enum
from kataloku_api.configs.global_const import DEFAULT_QUERY_OFFSET, DEFAULT_RETURN_COUNT
from config import SERVER_URL, AVATAR_UPLOAD_FOLDER, AVATAR_BASE_DIR, SHOWCASE_UPLOAD_FOLDER, SHOWCASE_BASE_DIR
from config import dbCon
import MySQLdb

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

app.config['UPLOAD_FOLDER'] = AVATAR_UPLOAD_FOLDER
app.config['SHOWCASE_UPLOAD_FOLDER'] = SHOWCASE_UPLOAD_FOLDER

__RESOURCE = 'business'


# GET BUSINESS CATEGORIES
@app.route(BASE_URL + __RESOURCE + '/categories', methods=['GET'])
def get_categories():
    obj = business.get_categories()
    return jsonify(obj)


@app.route(BASE_URL + __RESOURCE + '/<userkey>/<business_id>', methods=['GET'])
def get_business(userkey, business_id):
    obj = business.get_business(userkey, business_id)
    return jsonify(obj)


def get_business_json_by_owner(user_id):
    json_result = business.get_business_json_by_owner(user_id)
    # user_id = request.form.get('user_id', '')

    # json_result = {}

    result = Business.query.filter_by(user_id=user_id).first()
    if result:
        # category
        category = BusinessCategory.query.filter_by(id=result.category_id).first()
        obj_cat = {
            'id': category.id,
            'name': category.name,
            'slug': category.slug,
            'type_id': category.type_id
        }

        # showcase
        showcases = Picture.query.filter_by(business_id=result.id).order_by(Picture.id.desc()).limit(3)
        business_showcases = []
        i = 0
        for showcase in showcases:
            if i < 3:
                if showcase.picture is not None:
                    showcase_url = SERVER_URL + showcase.picture
                else:
                    showcase_url = ''
                obj_showcase = {
                    'id': showcase.id,
                    'picture': showcase_url
                }
                business_showcases.append(obj_showcase)
                i += 1
            else:
                break


        # items
        items = Item.query.filter_by(business_id=result.id).order_by(Item.id.desc()).limit(3)
        business_items = []
        i = 0
        for item in items:
            if i < 3:
                if item.photo is not None:
                    item_photo = SERVER_URL + item.photo
                else:
                    item_photo = ''

                obj_item = {
                    'id': item.id,
                    'name': item.name,
                    'price': item.price,
                    'unit': item.unit,
                    'description': item.description,
                    'category_id': item.category_id,
                    'business_id': item.business_id,
                    'picture': item_photo,
                    'type': item.type
                }
                business_items.append(obj_item)
                i += 1
            else:
                break

        json_result = {
            'id': result.id,
            'name': result.business_name,
            'description': result.description,
            'category': obj_cat,
            'address': result.address,
            'city': result.city,
            'postal_code': result.postal_code,
            'phone': result.phone,
            'avatar': SERVER_URL + result.avatar,
            'website': result.website,
            'province': result.province,
            'tag': result.tags,
            'showcases': business_showcases,
            'items': business_items,
            'first_use': result.first_use,
            'operation_days': result.operation_days,
            'lat': result.latitude or 0.0,
            'lng': result.longitude or 0.0,
            'created_at': result.create_date}

    return json_result


def nearby_business_to_json(user_id, business):
    # category
    category = BusinessCategory.query.filter_by(id=business.category_id).first()
    if category is not None:
        obj_cat = {
            'id': category.id,
            'name': category.name,
            'slug': category.slug,
            'type_id': category.type_id
        }
    else:
        obj_cat = {

        }


    # showcase
    showcases = Picture.query.filter_by(business_id=business.id).order_by(Picture.id.desc()).limit(3)
    business_showcases = []
    i = 0
    for showcase in showcases:
        if i < 3:
            if showcase.picture is not None:
                showcase_url = SERVER_URL + showcase.picture
            else:
                showcase_url = ''
            obj_showcase = {
                'id': showcase.id,
                'picture': showcase_url
            }
            business_showcases.append(obj_showcase)
            i += 1
        else:
            break


    # items
    items = Item.query.filter_by(business_id=business.id).order_by(Item.id.desc()).limit(3)
    business_items = []
    i = 0
    for item in items:
        if i < 3:
            if item.photo is not None:
                item_photo = SERVER_URL + item.photo
            else:
                item_photo = ''

            obj_item = {
                'id': item.id,
                'name': item.name,
                'price': item.price,
                'unit': item.unit,
                'description': item.description,
                'category_id': item.category_id,
                'business_id': item.business_id,
                'picture': item_photo,
                'type': item.type
            }
            business_items.append(obj_item)
            i += 1
        else:
            break

    tag_business = []
    # for tag in business.tags:
    #     obj_item = {
    #         'id': tag.id,
    #         'name': tag.name
    #     }
    #     tag_business.append(obj_item)
    #     i += 1


    # tags = BusinessTag.query.filter_by(business_id=business.id).all()
    #
    # tag_business = []
    # for tag in tags:
    #     obj_item = {
    #         'id': tag.id,
    #         'name': tag.name
    #     }
    #     tag_business.append(obj_item)
    #     i += 1

    json_result = {'id': business.id,
                   'name': business.business_name,
                   'description': business.description,
                   'category': obj_cat,
                   'address': business.address,
                   'city': business.city,
                   'postal_code': business.postal_code,
                   'phone': business.phone,
                   'avatar': SERVER_URL + (business.avatar or ''),
                   'website': business.website,
                   'province': business.province,
                   'tag': tag_business,
                   'operation_days': business.operation_days,
                   'lat': business.latitude or 0.0,
                   'lng': business.longitude or 0.0,
                   'distance': business.distance,
                   'created_at': business.create_date,
                   'showcases': business_showcases,
                   'items': business_items
                   }

    # if owner, show private informations
    if user_id == business.user_id:
        json_result['first_use'] = business.first_use

    return json_result


def business_to_json(user_id, business):
    json_result = business.business_to_json()
    return json_result


@app.route(BASE_URL + __RESOURCE + '/register/<userkey>', methods=['POST'])
def register_business(userkey):
    obj = business.register_business(userkey)
    return jsonify(obj)


@app.route(BASE_URL + __RESOURCE + '/update/<userkey>/<business_id>', methods=['POST'])
def update_business(userkey, business_id):
    obj = business.update_business(userkey, business_id)
    return jsonify(obj)


@app.route(BASE_URL + __RESOURCE + '/nearby/<userkey>/<distance>/<lat>/<lng>/<page>', methods=['GET'])
def show_nearby(userkey, distance, lat, lng, page):
    obj = business.show_nearby(userkey, distance, lat, lng, page)
    return jsonify(obj)


@app.route(BASE_URL + __RESOURCE + '/detail/<userkey>/<business_id>/<ref>', methods=['GET'])
def detail_business(userkey, business_id, ref):
    obj = business.detail_business(userkey, business_id, ref)
    return jsonify(obj)


@app.route(BASE_URL + __RESOURCE + '/navigation/log/<id>/<business_id>/<distance>', methods=['GET'])
def navigation_log(id, business_id, distance):
    obj = business.navigation_log(id, business_id, distance)
    return jsonify(obj)


def get_business_json_by_id(business_id):
    json_result = business.get_business_json_by_id(business_id)
    return json_result


@app.route(BASE_URL + __RESOURCE + '/showcase/<userkey>/<business_id>', methods=['GET'])
def get_showcase(userkey, business_id):
    obj = business.get_showcase(userkey, business_id)
    return jsonify(obj)


@app.route(BASE_URL + __RESOURCE + '/add_showcase/<userkey>/<business_id>', methods=['POST'])
def upload_showcase(userkey, business_id):
    obj = business.upload_showcase(userkey, business_id)
    return jsonify(obj)

@app.route(BASE_URL + __RESOURCE + '/bookmark/<userkey>/<business_id>', methods=['GET'])
def bookmark_business(userkey, business_id):
    obj = business.bookmark_business(userkey, business_id)
    return jsonify(obj)

@app.route(BASE_URL + __RESOURCE + '/bookmarked/<userkey>', methods=['GET'])
def get_bookmarked_business(userkey):
    obj = business.get_bookmarked_business(userkey)
    return jsonify(obj)


@app.route(BASE_URL + __RESOURCE + '/delete_null', methods=['GET'])
def delete_null():
    businesses = Business.query.filter_by(business_name=None)
    for business in businesses:
        db.session.delete(business)

    db.session.commit()
    return 'deleted'
