__author__ = 'ikhsanudinhakim'

from kataloku_api.models import Item
from flask import request, jsonify
from kataloku_api.utils.exceptions import InvalidUsage
from kataloku_api import app
from kataloku_api.models import User,db, Offering
from kataloku_api.configs.rest_config import BASE_URL
from kataloku_api.dbhelper import offering
from werkzeug.utils import secure_filename
from kataloku_api.configs import global_const
import os
from kataloku_api.utils.utilities import verify_post_params, echo_response, enum
from kataloku_api.utils.utilities import verify_post_params, echo_response
from config import SERVER_URL, ITEM_UPLOAD_FOLDER, ITEM_BASE_DIR
from datetime import datetime, date, timedelta
from config import dbCon
import MySQLdb

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

app.config['ITEM_UPLOAD_FOLDER'] = ITEM_UPLOAD_FOLDER

__RESOURCE = 'offering'

offering_type = enum(NEW=1, DISCOUNT=2, SPECIAL_OFFER='3')

@app.route(BASE_URL + __RESOURCE + '/item/list/<userkey>/<business_id>/<type>', methods=['GET'])
def get_item_for_offering(userkey, business_id, type):
    obj = offering.get_item_for_offering(userkey, business_id, type)
    return jsonify(obj)


@app.route(BASE_URL + __RESOURCE + '/create/<userkey>', methods=['POST'])
def make_offering(userkey):
    result = offering.make_offering(userkey)
    return jsonify(result)

@app.route(BASE_URL + __RESOURCE +'/nearby/<userkey>/<distance>/<lat>/<lng>/<page>', methods=['GET'])
def offering_nearby(userkey, distance, lat, lng, page):
    result = offering.offering_nearby(userkey, distance, lat, lng, page)
    return jsonify(result)





def is_userkey_valid(userkey):
    return offering.is_userkey_valid(userkey)
