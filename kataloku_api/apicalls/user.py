from flask import request, jsonify

from kataloku_api import app
from kataloku_api.models import User, db
from kataloku_api.apicalls.business import get_business_json_by_owner
from kataloku_api.dbhelper import user
from kataloku_api.utils.exceptions import InvalidUsage
from kataloku_api.utils.rest_handler import request_failed
from kataloku_api.utils.utilities import verify_post_params, echo_response, enum
from kataloku_api.configs.rest_config import BASE_URL
from werkzeug.utils import secure_filename
import pprint

import os
from config import SERVER_URL, AVATAR_UPLOAD_FOLDER, AVATAR_BASE_DIR

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

app.config['AVATAR_UPLOAD_FOLDER'] = AVATAR_UPLOAD_FOLDER

__RESOURCE = 'user'

import hashlib
import random

#REGISTER
@app.route(BASE_URL + __RESOURCE + '/register', methods=['POST'])
def register_user():
    obj = user.register_user()
    return jsonify(obj)


#LOGIN USING USERNAME
@app.route(BASE_URL + __RESOURCE + '/login', methods=['POST'])
def login():
    obj = user.login()
    return jsonify(obj)


@app.route(BASE_URL + __RESOURCE +'/social_check', methods=['POST'])
def social_auth_check():
    obj = user.social_auth_check()
    return jsonify(obj)


#GET USER
@app.route(BASE_URL + __RESOURCE + '/<userkey>/<user_id>', methods=['GET'])
def get_user(userkey, user_id):
    obj = user.get_user(userkey, user_id)
    return jsonify(obj)


#UPDATE PROFILE
@app.route(BASE_URL + __RESOURCE + '/profile_update/<userkey>', methods=['POST'])
def update_profiles(userkey):
    obj = user.update_profiles(userkey)
    return jsonify(obj)

@app.route(BASE_URL + __RESOURCE + '/dashboard_current_stat/count/<userkey>', methods=['GET'])
def dashboard_current_count_stat(userkey):
    obj = user.dashboard_current_count_stat(userkey)
    return jsonify(obj)



def get_user_json(entity):
    return user.get_user_json(entity)


def is_username_exist(username):
    return user.is_username_exist(username)


def is_email_exist(email):
    return user.is_email_exist(email)


def is_userkey_valid(userkey):
    return user.is_userkey_valid(userkey)

