from flask import request, jsonify
from kataloku_api import app
from flask import make_response
from kataloku_api.configs.global_const import DEBUG_MODE


#verify required POST params
def verify_post_params(required_fields):
    error = False
    error_fields = ''
    request_params = request.form

    for field in required_fields:
        if request_params.get(field) is None or request_params.get(field) == '':
            error = True
            error_fields += field + ','

    if error:
        status = 500
        #message = 'Required field(s) ' + substr($error_fields, 0, -2) . ' is missing or empty';
        message = 'required param(s): ' + error_fields + ' is missing' if DEBUG_MODE else 'invalid request'
        return echo_response(status, message)


#verify required GET params
def verify_get_params(required_fields):
    error = False
    error_fields = ''
    request_params = request.form

    for field in required_fields:
        if request_params.get(field) is None:
            error = True
            error_fields += field + ','

    if error:
        status = 500
        #message = 'Required field(s) ' + substr($error_fields, 0, -2) . ' is missing or empty';
        message = 'required param(s): ' + error_fields + ' is missing' if DEBUG_MODE else 'invalid request'
        return echo_response(status, message)


def enum(**enums):
    return type('Enum', (), enums)

def echo_response(status, message):
    return {'status': status, 'message': message}

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

@app.errorhandler(401)
def not_found(error):
    return jsonify({'status': 401, 'message': 'access restricted'})

@app.errorhandler(500)
def not_found(error):
    return jsonify({'status': 500, 'message': 'internal server error'})