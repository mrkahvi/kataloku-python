from kataloku_api import app
from flask import jsonify
from flask import make_response
#from flask.ext.httpauth import HTTPBasicAuth


#auth = HTTPBasicAuth()

#@app.errorhandler(404)


def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 200)

app.error_handler_spec[None][404] = not_found

#@auth.get_password
#def get_password(username):
#    if username == 'miguel':
#        return 'python'
#    return None

#@auth.error_handler
#def unauthorized():
#    return make_response(jsonify({'error': 'Unauthorized access'}), 403)

#@app.error_handler
def missing_params():
    return make_response(jsonify({'message': 'Failed. Missing parameters', 'status':500}), 200)


#@app.error_handler
def request_failed():
    return make_response(jsonify({'message': 'Request failed', 'status':400}), 200)