from flask import request, jsonify, json, render_template, make_response

from slugify import slugify
from kataloku_api.utils.exceptions import InvalidUsage
from kataloku_api import app
from kataloku_api.utils.rest_handler import request_failed
from kataloku_api.models import db, Category, User, BusinessTag, Business, BusinessCategory, Picture, Item
from kataloku_api.configs.rest_config import BASE_URL
from kataloku_api.dbhelper import business, item, offering
from werkzeug.utils import secure_filename
import os
from kataloku_api.utils.utilities import verify_post_params, echo_response, enum
from kataloku_api.configs.global_const import DEFAULT_QUERY_OFFSET, DEFAULT_RETURN_COUNT
from config import SERVER_URL, AVATAR_UPLOAD_FOLDER, AVATAR_BASE_DIR, SHOWCASE_UPLOAD_FOLDER, SHOWCASE_BASE_DIR
from config import dbCon
import MySQLdb
import pprint

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

app.config['UPLOAD_FOLDER'] = AVATAR_UPLOAD_FOLDER
app.config['SHOWCASE_UPLOAD_FOLDER'] = SHOWCASE_UPLOAD_FOLDER

__RESOURCE = ''

def create_app():
    Bootstrap(app)
    return app


#GET HOME
@app.route('/web', methods=['GET'])
def home():
    response = make_response(render_template('index.html', title='Home'))
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response


@app.route(__RESOURCE + '/nearby/<userkey>/<distance>/<lat>/<lng>/<page>', methods=['GET'])
def nearby_list(userkey, distance, lat, lng, page):
    nearby_json = business.show_nearby(userkey, distance, lat, lng, page)
    print nearby_json
    response = make_response(render_template('blocks/nearby_list.html',
        businesses=nearby_json['business'],
        businesses_dump=json.dumps(nearby_json['business'])))
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response


@app.route(__RESOURCE + '/offering/nearby/<userkey>/<distance>/<lat>/<lng>/<page>', methods=['GET'])
def offering_list(userkey, distance, lat, lng, page):
    offering_json = offering.offering_nearby(userkey, distance, lat, lng, page)
    print offering_json
    response = make_response(render_template('blocks/offering_list.html',
        offerings=offering_json['items'],
        offerings_dump=json.dumps(offering_json['items'])))
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response


@app.route(__RESOURCE + '/search/nearby/<userkey>/<keyword>/<distance>/<lat>/<lng>/<page>', methods=['GET'])
def search_list(userkey, keyword, distance, lat, lng, page):
    search_json = item.search_nearby(userkey, keyword, distance, lat, lng, page)
    return render_template('blocks/nearby_list.html',
        businesses=search_json['results'],
        businesses_dump=json.dumps(search_json['results']))


#GET MAP
@app.route('/map', methods=['GET'])
def nearby():
	return render_template('index.html', title='Home')