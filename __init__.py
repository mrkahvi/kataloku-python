from flask import request, jsonify, json
from kataloku_api.models import Item, User, Category, BusinessCategory, Business, BusinessTag, BusinessViewLog, Review, Picture, ItemListViewLog, SearchLog, ItemViewLog, Offering, NavigationLog, NearbyLog, db
from kataloku_api.exceptions import InvalidUsage, handle_invalid_usage
from datetime import datetime, date, timedelta
from slugify import slugify
import uuid

from werkzeug.utils import secure_filename
import os

import pprint
import MySQLdb
import gc
from kataloku_api import app
from config import dbCon, UPLOAD_FOLDER

from kataloku_api.apicalls import user, item, review, offering
from kataloku_api.views import home

from flask.ext.script import Server, Manager
from flask.ext.migrate import Migrate, MigrateCommand


ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '+' in filename and \
        filename.rsplit('+', 1)[1] in ALLOWED_EXTENSIONS


#GET INDEX
@app.route('/', methods=['GET'])
def index():
  return 'ok'


@app.route('/move/latlng/pkl_to_solo', methods=['GET'])
def move_latlng_pkl_to_solo():
  pprint.pprint(request.data)
  businesses = Business.query.all()
  for business in businesses:
    if business.latitude != None and business.longitude != None:
      new_lat = float(business.latitude) + 0.2158924
      new_lng = float(business.longitude) + 0.3070979
      print new_lat
      business.latitude = str(new_lat)
      business.longitude = str(new_lng)
  db.session.commit() 
  return jsonify({ 'status': 200, 'changed': True })


if __name__ == '__main__':
  app.config.update(
    DEBUG=True,
    PROPAGATE_EXCEPTIONS=True,
  )
  
  server = Server(host="0.0.0.0", port=5000)
  migrate = Migrate(app, db)

  manager = Manager(app)
  manager.add_command('db', MigrateCommand)
  manager.add_command("runserver", server)

  manager.run()

# @app.route('/social_check', methods=['POST'])
# def social_check():
#       # if request.method == 'POST':
#     social_id = request.form.get('social_id', '')
#     push_id = request.form.get('push_id', '')
#     type = request.form.get('type', '')
#     pprint.pprint(social_id)

#     entity = None
#     if type == '1':
#         entity = User.query.filter_by(fb_id=social_id).first()
#         pprint.pprint('1')
#     elif type == '2':
#         entity = User.query.filter_by(twitter_id=social_id).first()
#         pprint.pprint('2')

#     if entity is None:
#         response_code = 204
#         message = 'empty'
#         user = {}
#     else:
#         entity.push_id = push_id
#         db.session.add(entity)
#         db.session.commit()

#         response_code = 200
#         message = 'exist'
#         user = {
#             'id': entity.id or '',
#             'username': entity.username or '',
#             'first_name': entity.first_name or '',
#             'last_name': entity.last_name or '',
#             'business_name': entity.business_name or '',
#             'email': entity.email or '',
#             'fb_id': entity.fb_id or '',
#             'api_key': entity.api_key or '',
#             'avatar': entity.avatar or '',
#             'category': entity.category.id or '',
#             'city': entity.city or '',
#             'phone': entity.phone or '',
#             'address': entity.address or '',
#             'lng': entity.longitude or '',
#             'lat': entity.latitude or ''
#         }

#     obj = {
#         'status': response_code,
#         'message': message,
#         'user': user
#     }
#     return jsonify(obj)


# @app.route('/login', methods=['POST'])
# def email_login():
#     pprint.pprint(request.data)
#       # if request.method == 'POST':
#     username = request.form.get('username', '')
#     password = request.form.get('password', '')
#     push_id = request.form.get('push_id', '')
#     entity = User.query.filter_by(username=username, password=password).first()

#     message = 'failed'
#     if entity:
#         message = 'success'
#     else:
#         message = 'failed'

#     user = {}

#     if message == 'success':
#         entity.push_id = push_id
#         db.session.add(entity)
#         db.session.commit()

#         response_code = 200
#         message = 'success'
#         user = {
#             'id': entity.id,
#             'username': username or '',
#             'first_name': entity.first_name or '',
#             'last_name': entity.last_name or '',
#             'business_name': entity.business_name or '',
#             'email': entity.email or '',
#             'fb_id': entity.fb_id or '',
#             'twitter_id': entity.twitter_id or '',
#             'api_key': entity.api_key or '',
#             'avatar': entity.avatar or '',
#             'category': entity.category.id or '',
#             'city': entity.city or '',
#             'phone': entity.phone or '',
#             'address': entity.address or '',
#             'lng': entity.longitude or '',
#             'lat': entity.latitude or ''
#         }
#     else:
#         response_code = 204
#         message = 'failed'

#     obj = {
#         'status': response_code,
#         'message': message,
#         'user': user
#     }
#     return jsonify(obj)


@app.route('/detail/<userId>/<id>/<ref>/<lng>/<lat>', methods=['GET'])
def show_from_customer(userId, id, ref, lng, lat):

    query = 'SELECT *, ( 6371 * acos( cos( radians('+lat+' ) ) * cos( radians(latitude) ) * cos( radians( longitude ) - radians('+lng+') ) + sin( radians('+lat+' ) ) * sin(radians(latitude)) ) ) AS distance FROM User WHERE id='+id+' LIMIT 1'

    pprint.pprint(query)

    cur = dbCon.cursor(MySQLdb.cursors.DictCursor)
    cur.execute(query)
    results = cur.fetchall()

    for row in results:
      user = {
        'id': row["id"],
        'username': row["username"] or '',
        'first_name': row["first_name"] or '',
        'last_name': row["last_name"] or '',
        'avatar': '/uploads/documents/'+ (row["avatar"] or '')
      }

      entity = User.query.get(id)
      seekUser = User.query.get(userId)

      if id != userId:
        view_log = BusinessViewLog()
        view_log.date = datetime.now()
        view_log.user = seekUser
        view_log.business = entity
        view_log.ref = ref
        db.session.add(view_log)
            
      pictures = entity.pictures
      photos = []

      i= 0
      for picture in pictures:
        photo = {
          'id': picture.id,
          'name': '/uploads/documents/'+ (picture.avatar or '')
        }
        photos.append(photo)

      user['showcase_pictures'] = photos

      cat = BusinessCategory.query.get(int(row['category_id']))

      if cat != None:
        user['category'] = cat.name or ''
      else:
        user['category'] = cat

      user['business_name'] = row['business_name'] or ''
      user['description'] = row['description'] or ''
      user['kataloku_page'] = 'http://kataloku.com/business/'+ (row['slug'] or '')
      user['city'] = row['city'] or ''
      user['phone'] = row['phone'] or ''
      user['address'] = row['address'] or ''
      user['lat'] = row['latitude'] or ''
      user['lng'] = row['longitude'] or ''
      user['distance'] = row['distance'] or ''
      user['operation_days'] = row['operation_days'] or ''

      products = []
      items = Item.query.filter_by(user_id=row['id']).order_by(Item.id.desc()).limit(3)
      i= 0
      for item in items:
          if i < 3:
              product = {
                'id': item.id,
                'name': item.name or '',
                'photo': '/uploads/documents/'+ (item.photo or ''),
                'price': item.price or ''
              }
                    
              if item.category != None:
                  product['category'] = item.category.name or ''
              else:
                  product['category'] = item.category or ''
              
              product['unit'] = item.unit or ''
              products.append(product)
              i += 1
          else:
              break
      
      user['items'] = products
      db.session.commit()

      # dbCon.close()
      gc.collect()

      if len(results) > 0:
        return jsonify({ 'status': 200, 'business': user})
      else:
        return jsonify({ 'status': 204, 'business': user})


# @app.route('/item/search/nearby/<userkey>/<keyword>/<distance>/<lng>/<lat>/<page>', methods=['GET'])
# def search_nearby(userkey, keyword, distance, lng, lat, page):
#     end = 20 * int(page)
#     start = end - 20

#     query = "SELECT Item.id AS item_id, Item.price AS price, User.id AS user_id, Item.name AS name, Intem.photo AS photo, User.business_name AS business_name, Item.unit AS unit, address, longitude, latitude, ( 6371 * acos( cos( radians(" + lat + " ) ) * cos( radians(User.latitude) ) * cos( radians( User.longitude ) - radians(" + lng + ") ) + sin( radians(" + lat + " ) ) * sin(radians(User.latitude)) ) ) AS distance FROM Item LEFT JOIN User ON Item.user_id = User.id WHERE Item.description LIKE '%" + keyword + "%' OR Item.name LIKE '%" + keyword + "%' HAVING distance <= " + distance + " OR distance = 0 ORDER BY distance ASC LIMIT " + str(start) + " , " + str(end)

#     pprint.pprint(query)

#     cur = dbCon.cursor(MySQLdb.cursors.DictCursor)
#     cur.execute(query)
#     results = cur.fetchall()

#     items = []

#     i = 0
#     for row in results:
#       item = {
#         'item_id': row['item_id'] or '',
#         'name': row['name'] or '',
#         'user_id': row['user_id'] or '',
#         'business_name': row['business_name'] or '',
#         'price': row['price'] or '',
#         'unit': row['unit'] or '',
#         'lat': row['latitude'] or '',
#         'lng': row['longitude'] or '',
#         'distance': row['distance'] or '',
#         'address': row['address'] or '',
#         'photo': "/uploads/documents/" + (row['photo'] or ''),
#         'distance': distance or ''
#       }
#       items.append(item)

#       businessUser = User.query.get(row['user_id'])
#       user = User.query.get(userId)

#       if id != userId:
#         view_log = SearchLog()
#         view_log.date = datetime.now()
#         view_log.user = user
#         view_log.business_user = businessUser
#         view_log.keyword = keyword
#         view_log.distance = distance
#         view_log.order_id = i + start
#         db.session.add(view_log)
#         i = i + 1
        
#     db.session.commit()

#     if len(items) > 0:
#       return jsonify({ 'status': 200, 'results': items})
#     else:
#       return jsonify({ 'status': 204, 'results': []})


# @app.route('/review/list/<businessUserId>/<page>', methods=['GET'])
# def review_list(businessUserId, page):
#     # end = 20 * page
#     # start = end - 20
#     entities = Review.query.filter_by(business_user_id=businessUserId).order_by(Review.id.desc()).limit(20)

#     reviews = []
#     for entity in entities:
#       review = {}
#       review['user_id'] = entity.user_id
#       review['user_name'] = entity.user.first_name + ' ' + entity.user.last_name
#       review['avatar'] = '/uploads/documents/' + entity.user.avatar
#       review['business_user_id'] = entity.business_user.id
#       review['message'] = entity.message
#       review['time'] = entity.time.strftime('%Y-%m-%d %H:%M:%S')
#       review['rating'] = entity.rating
#       reviews.append(review)

#     if len(review) > 0:
#       return jsonify({'status': 200, 'message': 'success', 'reviews': reviews })
#     else:
#       return jsonify({'status': 204, 'message': 'empty', 'review': [] })

# @app.route('/review/last_list/<businessUserId>', methods=['GET'])
# def last_review_list(businessUserId):
#     # end = 20 * page
#     # start = end - 20
#     entities = Review.query.filter_by(business_user_id=businessUserId).order_by(Review.id.desc()).limit(3)

#     reviews = []
#     for entity in entities:
#       review = {}
#       review['user_id'] = entity.user_id
#       review['user_name'] = entity.user.first_name + ' ' + entity.user.last_name
#       review['avatar'] = '/uploads/documents/' + entity.user.avatar
#       review['business_user_id'] = entity.business_user.id
#       review['message'] = entity.message
#       review['time'] = entity.time.strftime('%Y-%m-%d %H:%M:%S')
#       review['rating'] = entity.rating
#       reviews.append(review)

#     if len(review) > 0:
#       return jsonify({'status': 200, 'message': 'success', 'reviews': reviews })
#     else:
#       return jsonify({'status': 204, 'message': 'empty', 'review': [] })


# @app.route('/dashboard_current_stat/count/<businessUserId>', methods=['GET'])
# def dashboard_current_count_stat(businessUserId):
#     # end = 20 * page
#     today = date.today()
#     tomorrow = today + timedelta(days=1)
#     pprint.pprint(today)

#     nearby = NearbyLog.query.filter(NearbyLog.date >= today, NearbyLog.date < tomorrow, NearbyLog.business_user_id == businessUserId).count()
#     search = SearchLog.query.filter(SearchLog.date >= today, SearchLog.date < tomorrow, SearchLog.business_user_id == businessUserId).count()
#     businessView = BusinessViewLog.query.filter(BusinessViewLog.date >= today, BusinessViewLog.date < tomorrow, BusinessViewLog.business_user_id == businessUserId).count()
#     itemListView = ItemListViewLog.query.filter_by(date=today, business_user_id=businessUserId).count()
#     # itemView = ItemViewLog.query.filter(ItemViewLog.date >= today, ItemViewLog.date < tomorrow, ItemViewLog.user_id == businessUserId).count()
#     itemView = db.session.query(ItemViewLog).join(Item).filter(ItemViewLog.date >= today, ItemViewLog.date < tomorrow, Item.user_id == businessUserId).count()
#     navigation = NavigationLog.query.filter(NavigationLog.date >= today, NavigationLog.date < tomorrow, NavigationLog.business_user_id == businessUserId).count()

#     logs = {}
#     logs['nearbies'] = nearby
#     logs['searches'] = search
#     logs['business_views'] = businessView
#     logs['item_browses'] = itemListView
#     logs['item_views'] = itemView
#     logs['navigations'] = navigation

#     result = {"status": 200,  "results": logs}
#     return jsonify(result)


# @app.route('/dashboard_current_stat/<businessUserId>', methods=['GET'])
# def dashboard_current_detail_stat(businessUserId):
#     today = date.today()
#     tomorrow = today + timedelta(days=1)
#     pprint.pprint(today)

#     searchLogs = SearchLog.query.filter(SearchLog.date >= today, SearchLog.date < tomorrow, SearchLog.business_user_id == businessUserId)

#     items = []
#     for searchLog in searchLogs:
#         logs = {}
#         logs['user'] = searchLog.user_id
#         items.append(logs)

#     result = {"status": 200,  "results": items}
#     return jsonify(result)

# @app.route('/review/write', methods=['POST'])
# def review_create():
#     user_id = request.form.get('user_id')
#     business_user_id = request.form.get('business_user_id')
#     message = request.form.get('message')
#     rating = request.form.get('rating')

#     try:
#       entity = Review()

#       entity.user_id = user_id
#       entity.time = datetime.now()
#       entity.business_user_id = business_user_id
#       entity.message = message
#       entity.rating = int(rating)
#       db.session.add(entity)
#       db.session.commit()

#       review = {}
#       review['user_id'] = entity.user_id
#       review['business_user_id'] = entity.business_user_id
#       review['time'] = entity.time.strftime('%Y-%m-%d %H:%M:%S')
#       review['message'] = entity.message
#       review['rating'] = entity.rating
#       message = "success"
#       result = {"status" : 200,  "message" : message, "review" : review } 
#       return jsonify(result)
#     except InvalidUsage:
#       result = {"status" : 200,  "message" : "error" }
#       return jsonify(result)


# @app.route('/offering/item/list/<businessUserId>/<type>/<page>', methods=['GET'])
# def item_list_offering(businessUserId, type, page):
#   pprint.pprint(businessUserId)
#   # if request.method == 'POST':

#   if type == 1:
#     items = Item.query.filter_by(user_id=businessUserId).order_by(Item.id.desc()).limit(20)
#   else:
#     query = 'SELECT * FROM Item WHERE user_id=' + businessUserId + ' AND price > 0'

#     items = Item.query.from_statement(query).all()

#   objs = []
#   for item in items:
#       obj = {}
#       obj['id'] = item.id
#       obj['name'] = item.name or ''
#       obj['price'] = item.price or ''
#       obj['unit'] = item.unit or ''
#       obj['photo'] = '/uploads/documents/' + (item.photo or '')
#       objs.append(obj)
  
#   if len(objs) > 0:
#     return jsonify({'status': 200, 'message': 'message', 'items': objs })
#   else:
#     return jsonify({'status': 204, 'message': 'failed', 'items': [] })


# @app.route('/navigation/log/<id>/<userId>/<distance>', methods=['GET'])
# def navigation_log(id, userId, distance):
#     try:
#       navLog = NavigationLog()
#       navLog.date = datetime.now()
#       navLog.user_id = id
#       navLog.business_user_id = userId
#       pprint.pprint(distance)
#       navLog.distance = round(float(distance), 2)
#       db.session.add(navLog)
#       db.session.commit()

#       result={"status" : 200,  "message" : 'success'}

#       return jsonify(result)
#     except InvalidUsage:
#       result={"status" : 400,  "message" : "error"}
#       return jsonify(result)


# @app.route('/offering/create', methods=['POST'])
# def create_offering():
#     itemId = request.form.get('item_id')
#     type = request.form.get('type')
#     message = request.form.get('message')
#     end = request.form.get('end')
#     end_nums = end.split(' ')
#     date = end_nums[0]
#     time = end_nums[1]

#     date_num = date.split('-')
#     time_num = time.split(':')

#     item = Item.query.get(itemId)

#     try:
#         entity = Offering()
#         entity.type = type
#         entity.item = item
#         entity.message = message
#         entity.start = datetime.now()
#         entity.end = datetime(int(date_num[0]), int(date_num[1]), int(date_num[2]), int(time_num[0]), int(time_num[1]))
#         db.session.add(entity)
#         db.session.commit()

#         message = "success"
#         obj = {}
#         obj['id'] = entity.id
#         obj['item_id'] = entity.item.id
#         obj['message'] = entity.message
#         obj['end'] = entity.end.strftime('%Y-%m-%d %H:%M:%S')
         
#         result = {"status" : 200,  "message" : message, "offering" : obj}

#         return jsonify(result)
#     except InvalidUsage:
#         result = {"status" : 204,  "message" : message }
#         return jsonify(result)


# @app.route('/user_remove', methods=['POST'])
# def user_remove(id):
#   pprint.pprint(request.data)
#   user = User.query.get(id)
#   db.session.delete(user)
#   db.session.commit()
#   return jsonify({ 'status': 200 })

# @app.route('/business/open', methods=['POST'])
# def business_open():
#   pprint.pprint(request.data)
#   id = request.form.get('id')
#   user = User.query.get(id)
#   user.open = True
#   db.session.commit()
#   return jsonify({ 'status': 200, 'open': True })

# @app.route('/business/close', methods=['POST'])
# def business_close():
#   pprint.pprint(request.data)
#   id = request.form.get('id')
#   user = User.query.get(id)
#   user.open = False
#   db.session.commit()
#   return jsonify({ 'status': 200, 'open': False })

# @app.route('/move/latlng/jogja_to_pkl', methods=['GET'])
# def move_latlng_jogja_to_pkl():
#   pprint.pprint(request.data)
#   businesses = Business.query.all()
#   for business in businesses:
#     if business.latitude != None and business.longitude != None:
#       new_lat = float(business.latitude) + 0.731665487
#       new_lng = float(business.longitude) - 0.824667459
#       print new_lat
#       business.latitude = str(new_lat)
#       business.longitude = str(new_lng)
#   db.session.commit()
#   return jsonify({ 'status': 200, 'changed': True })


# @app.route('/move/latlng/pkl_to_jogja', methods=['GET'])
# def move_latlng_pkl_to_jogja():
#   pprint.pprint(request.data)
#   businesses = Business.query.all()
#   for business in businesses:
#     if business.latitude != None and business.longitude != None:
#       new_lat = float(business.latitude) - 0.731665487
#       new_lng = float(business.longitude) + 0.824667459
#       print new_lat
#       business.latitude = str(new_lat)
#       business.longitude = str(new_lng)
#   db.session.commit()
#   return jsonify({ 'status': 200, 'changed': True })
