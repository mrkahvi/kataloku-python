Install python development package dan mysql untuk python:
```
#!terminal

sudo apt-get install build-essential python-dev libmysqlclient-dev
sudo apt-get install python-mysqldb
```

kalau belum punya virtualenv, run:
```
#!terminal

sudo apt-get install python-virtualenv
```

setelah pull, via terminal masuk ke folder hasil pull

run:

```
#!terminal

virtualenv venv
```

masuk ke mode virtual environment
run:


```
#!terminal

. venv/bin/activate
```

Project membutuhkan flask, flask-sqlalchemy, slugify, dan mysql
run:

```
#!terminal

pip install flask
```
run:

```
#!terminal

pip install flask-sqlalchemy
```

run:

```
#!terminal

pip install python-slugify
```

run:
```
#!terminal

pip install pymysql
```

run:
```
#!terminal

pip install flask-migrate
python __init__.py db init
```